module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{png,html,json,js}"
  ],
  "swDest": "./public/sw-conf.js",
  "swSrc": ""
};
