import React from 'react';
import './App.css';

import { Provider } from 'react-redux';
import { pm } from './layers/turtleos-pm/pm.jsx';
import GoogleFontLoader from 'react-google-font-loader';
import { TurtleOSViewport } from './layers/turtleos-vpm/vpm.jsx';

function App() {
  return (
      <div className="App" style={{
          height: '100vh',
          width: '100%',
          userSelect:'none'
      }}>
        <GoogleFontLoader fonts={[
            {
                font:'Shadows Into Light',
                weights:[400]
            }
        ]}/>
        <Provider store={ pm }>
          <TurtleOSViewport />
      </Provider>
    </div>
  );
}

export default App;
