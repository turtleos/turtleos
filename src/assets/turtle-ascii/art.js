/*
  Found this awesome ASCII Art here:
  https://ascii.co.uk/art/turtle
*/

const TurtleASCII = `
     _____   ____
    /     \\ |  o |
   |       |/ __\\|
   |_________/
   |_|_| |_|_|

`;

export {
    TurtleASCII
}
