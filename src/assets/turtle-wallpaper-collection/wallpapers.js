
//
// ALPHA WALLPAPERS
//
// One of ElementaryOS default Wallpapers
import elementary from './wallpapers/elementary.jpg';

//
// V1 SMALLWORLD WALLPAPERS
//
//Pexel Wallappers
import v1pexelshouse from './wallpapers/pexels.lucas.craig.house.jpg';
import v1pexelsgardens1 from './wallpapers/pexels.pixabay.gardens1.jpg';
import v1pexelsgardens2 from './wallpapers/pexels.thanhhoa.tran.gardens2.jpg';
import v1pexelscliff from './wallpapers/pexels.adrien.olichon.cliff.jpg';
//Insta Wallpapers from generous photographers donating wallpapers
import v1instafield1 from './wallpapers/insta.jo_foto.field1.jpg';
import v1instaboat1 from './wallpapers/insta.jo_foto.boat1.jpg';

//
// V2 ONCEINALIFETIME WALLPAPERS
//
import v2pexelssunset from './wallpapers/pexels.sindre.strom.sunset.jpg';

const WALLPAPERS = {
    // Alpha Wallpaper
    elementary: {
        id:'elementary',
        title:'Elementary Loki',
        path: elementary,
        author: {
            name:'[unknown]',
            url:'[unknown]'
        },
        url:'[unknow]',
        theme:{
            id:'dark',
            textOnWallpaper:'white'
        },
        bg: {
            backgroundImage: `url(${elementary})`,
            backgroundPosition:'center',
            backgroundRepeat:'no-repeat',
            backgroundSize: 'cover'
        }
    },
    // V1 SmallWorld
    lonelyhouse: {
        id: 'lonelyhouse',
        title:'Cabin near lake',
        path: v1pexelshouse,
        author: {
            name:'Lucas Craig',
            url:'https://www.pexels.com/@lucascraig'
        },
        url:'https://www.pexels.com/photo/cabin-on-green-grass-field-near-lake-under-white-clouds-3689772/',
        theme: {
            id:'dark',
            textOnWallpaper:'white'
        },
        bg: {
            backgroundImage: `url(${v1pexelshouse})`,
            backgroundPosition:'center',
            backgroundRepeat:'no-repeat',
            backgroundSize: 'cover'
        }
    },
    gardens1: {
        id: 'gardens1',
        title:'Scenic View',
        path: v1pexelsgardens1,
        author: {
            name:'Pixabay',
            url:'https://www.pexels.com/@pixabay'
        },
        url: 'https://www.pexels.com/photo/scenic-view-of-rice-paddy-247599/',
        theme: {
            id:'dark',
            textOnWallpaper:'white'
        },
        bg: {
            backgroundImage: `url(${v1pexelsgardens1})`,
            backgroundPosition:'center',
            backgroundRepeat:'no-repeat',
            backgroundSize: 'cover'
        }
    },
    gardens2: {
        id: 'gardens2',
        path: v1pexelsgardens2,
        title:'Rice terraces',
        author: {
            name: 'thanhhoa tran',
            url:'https://www.pexels.com/@thanhhoa-tran-640546'
        },
        url: 'https://www.pexels.com/photo/view-of-rice-terraces-1447092/',
        theme: {
            id:'dark',
            textOnWallpaper:'white'
        },
        bg: {
            backgroundImage: `url(${v1pexelsgardens2})`,
            backgroundPosition:'center',
            backgroundRepeat:'no-repeat',
            backgroundSize: 'cover'
        }
    },
    persononcliff: {
        id:'persononcliff',
        title:'Person on field',
        path: v1pexelscliff,
        author: {
            name:'Adrien Olichon',
            url:'https://www.pexels.com/@adrien-olichon-1257089'
        },
        url:'https://www.pexels.com/photo/person-standing-on-green-grass-field-3709388/',
        theme: {
            id:'dark',
            textOnWallpaper:'white'
        },
        bg: {
            backgroundImage: `url(${v1pexelscliff})`,
            backgroundPosition:'center',
            backgroundRepeat:'no-repeat',
            backgroundSize: 'cover'
        }
    },
    field1: {
        id: 'field1',
        title:'Feldrand',
        path: v1instafield1,
        author:{
            name: 'jo_foto',
            url:'https://www.instagram.com/jo__foto/'
        },
        url:'https://www.instagram.com/p/CDGiqyUglSC/',
        theme: {
            id:'dark',
            textOnWallpaper:'white'
        },
        bg: {
            backgroundImage: `url(${v1instafield1})`,
            backgroundPosition:'center',
            backgroundRepeat:'no-repeat',
            backgroundSize: 'cover'
        }
    },
    boat1: {
        id: 'boat1',
        title:'Sonnenuntergang am Strand',
        path: v1instaboat1,
        url:'https://www.instagram.com/p/CA5Ra1ZAqi5/',
        author:{
            name: 'jo_foto',
            url:'https://www.instagram.com/jo__foto/'
        },
        theme: {
            id:'dark',
            textOnWallpaper:'white'
        },
        bg: {
            backgroundImage: `url(${v1instaboat1})`,
            backgroundPosition:'center',
            backgroundRepeat:'no-repeat',
            backgroundSize: 'cover'
        }
    },
    // v2 OnceInALifetime
    sunset: {
        id:'sunset',
        title:'Aerial Sunset',
        path: v2pexelssunset,
        author: {
            name:'Adrien Olichon',
            url:'https://www.pexels.com/@blitzboy'
        },
        url:'https://www.pexels.com/photo/aerial-photography-of-water-beside-forest-during-golden-hour-1144176/',
        theme: {
            id:'dark',
            textOnWallpaper:'white'
        },
        bg: {
            backgroundImage: `url(${v2pexelssunset})`,
            backgroundPosition:'center',
            backgroundRepeat:'no-repeat',
            backgroundSize: 'cover'
        }
    },
};

function isWallpaper(name) {
    if(Object.keys(WALLPAPERS).indexOf(name)===-1) {
        return false;
    } else {
        return true;
    }
}

let enforceWallpaper = "sunset";
function defaultWallpaper() {
    if(!!enforceWallpaper) return enforceWallpaper;
    return Object.keys(WALLPAPERS)[0];
}

export {
    WALLPAPERS,
    isWallpaper,
    defaultWallpaper
}
