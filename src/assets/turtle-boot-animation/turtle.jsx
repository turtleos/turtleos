import React from 'react';

import turtleBody from './assets/turtle.png';
import turtleFootLeft from './assets/turtle_f_l.png';
import turtleFootRight from './assets/turtle_f_r.png';
import turtleOverlay from './assets/turtle_over.png';

import './animation.css';

function TurtleAnimation() {
    const [opacity, setOpacity] = React.useState(0);
    setTimeout(()=>{
        if(opacity<3*3*2) {
            setOpacity(opacity+1);
        }
    },50);
    return (
        <div className="turtle">
          <img style={{opacity:opacity-(3*3)}} alt="turtle" src={turtleBody}/>
          <img style={{opacity:opacity-(2*3)}} alt="turtle-foot-left" src={turtleFootLeft}/>
          <img style={{opacity:opacity-(1*3)}} alt="turtle-foot-right" src={turtleFootRight}/>
          <img style={{opacity:opacity-(0.5*3)}}alt="turtle-overlay" src={turtleOverlay}/>
        </div>
    );
}

export {TurtleAnimation}
