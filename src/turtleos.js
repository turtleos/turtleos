const TURTLEOS = {
    release: {
        name: "ALongWayDown",
        prefix: "TurtleOS",
        build: "development",
        version: [0,3,0]
    }
};
export {
    TURTLEOS
}
