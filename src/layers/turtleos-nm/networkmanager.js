import { pm } from '../turtleos-pm/pm';
import { resolveUrl } from './networktools';

function NetworkManager() {
    return {
        fetch : (url, conf) => {
                if(!pm.getState().io.network) {
                    return new Promise((resolve, reject)=>reject({
                        msg:pm.getState().lang.translate('network.noconnection'),
                        error:true
                    }));
                } else {
                    return fetch(url, conf);
                }
        },
        resolve: (url) => {
            return resolveUrl(url);
        }
    };
}

export {
    NetworkManager
}
