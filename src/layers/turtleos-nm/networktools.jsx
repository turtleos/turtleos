import { pm } from '../turtleos-pm/pm';

function resolveUrl(url) {

    let vfs = pm.getState().vfs;
    let d = '';
    try {
        let file = vfs.readFileSync('/etc/hosts');
        d=file.data.content;
        d=JSON.parse(d);
    } catch(e) {
        return url;
    }
    if(url.indexOf('://')===-1) {
        return url;
    }
    let protocol = url.split("://")[0];
    let domain = url.slice(protocol.length+3);

    //url contains params but no end / replace ? with /
    if (domain.indexOf('/?') === -1) domain.replace('?', '/?');

    let new_domain = domain;

    for ( let route of d.routes ) {
        let r = route.origin.split("/");
        let d = domain.split("/");
        let t = route.to.split("/");
        t = t.map((val, index) => {
            if (val === '*') return d[index];
            return val;
        });
        let res = d.map((val, index) => {
            if(r[index] === '*') return val;
            if(r[index] === val) return t[index];
            if(index >= r.length && [...r].pop() === '*') return val;
            return undefined;
        });
        if(res.indexOf(undefined)===-1) {
            new_domain = res.join('/');
        }
    }


    return protocol + '://' + new_domain;

}

function isNetworkEnabled() {
    return pm.getState().io.network;
}

export {
    resolveUrl,
    isNetworkEnabled
}
