import React from 'react';

import { createStore } from 'redux';

import { BootSequence1 } from '../turtleos-bootloader/bootloader';

import { VirtFS } from '../../lib/virtfs/virtfs';

import { ROOTFS } from '../../lib/turtleos-rootfs/rootfs';

import { WALLPAPERS, isWallpaper, defaultWallpaper } from '../../assets/turtle-wallpaper-collection/wallpapers.js';

import { ActivityManager } from '../turtleos-am/am';

import { ViewIndexManager } from '../turtleos-vpm/vpm';

import { LANG } from '../../lib/turtleos-translations/translations';

import { NetworkManager } from '../turtleos-nm/networkmanager';

import { TSH } from '../tsh/tsh.js';

let backupFS = localStorage.getItem('turtleos-storage');

try {
    if(!backupFS) {
        throw new Error("FS Empty");
    }
    backupFS = JSON.parse(backupFS);
} catch (e) {
    backupFS = ROOTFS;
}

let vfs = new VirtFS({
    isroot: true,
    filesystem: backupFS
});

let vim = new ViewIndexManager(10);

const defaultProcesses = {
    vpm: <BootSequence1/>,
    vfs: vfs,
    tsh: TSH(),
    platform: 'auto',
    lang: LANG,
    nm: NetworkManager(),
    wallpaper: WALLPAPERS[defaultWallpaper()],
    io: {
        network: false,
        autonetwork:false,
        display: {
            brightness:50
        }
    },
    internal: {
        clock: new Date(),
        startup: new Date(),
        uptime: new Date() - new Date(),
        usage: 0
    },
    install:{detected:false},
    am: new ActivityManager(),
    vim: vim,
    update:{}
};

function reducer(state = defaultProcesses, action) {
    switch(action.type) {
    case 'internal:clock':
        return {
            ...state,
            internal: {
                ...state.internal,
                clock: action.payload.date,
                uptime: action.payload.date - state.internal.startup
            }
        };
    case 'internal:usage':
        return {
            ...state,
            internal: {
                ...state.internal,
                usage: action.payload.usage
            }
        };
    case 'install:hard-disk':
        return {
            ...state,
            install: {
                detected: action.payload.status,
                data: action.payload.data
            }
        };
    case 'install:reset':
        return {
            ...state,
           /* vfs: new VirtFS({
                isroot: true,
                filesystem: ROOTFS
                })*/
            vfs: 'reset'
        };
    case 'view:change':
        return {
            ...state,
            vpm: action.payload.view
        };
    case 'tsh:reload':
        return {
            ...state,
            tsh: TSH({
                bool:true,
                am: action.payload.am,
                tpm: action.payload.tpm
            })
        };
    case 'platform:update':
        return {
            ...state,
            platform: action.payload.platform
        };
    case 'lang:change':
        return {
            ...state,
            lang: {
                ...state.lang,
                lang:action.payload.lang
            }
        };
    case 'wallpaper:change':
        let wp = action.payload.wallpaper;
        if(!isWallpaper(wp)) {
            wp=defaultWallpaper();
        }
        return {
            ...state,
            wallpaper: WALLPAPERS[wp]
        };
    case 'io:update':
        return {
            ...state,
            io: {
                ...state.io,
                [action.payload.interface]:action.payload.state
            }
        };
    case 'io:update-depth':
        return {
            ...state,
            io: {
                ...state.io,
                [action.payload.interface]: {
                    ...state.io[action.payload.interface],
                    [action.payload.key]:action.payload.value
                }
            }
        };
    case 'am:update':
        return {
            ...state,
            am: {
                ...state.am,
                windows: action.payload.windows}
        };
    case 'am:hide-window':
        return {
            ...state,
            am: {
                ...state.am,
                hidden: {
                    ...state.am.hidden,
                    [action.payload.id]:action.payload.state
                }
            }
        };
    case "turtleos:new-update": {
        return {
            ...state,
            update:action.payload.changelog
        };
    }
    default:
        return state;
    }
}

const pm = createStore(reducer);

export {
    pm
};
