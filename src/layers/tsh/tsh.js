import { JLI } from '../../lib/jli/jli.js';
import { LISP_COMMANDS as LISP, LISP_BLACKLIST } from '../../lib/jli/lisp.js';
import { TURTLEOS } from '../../turtleos.js';
import { pm } from '../turtleos-pm/pm';
import { WALLPAPERS, isWallpaper, defaultWallpaper } from '../../assets/turtle-wallpaper-collection/wallpapers.js';
import { IO } from '../../lib/turtleos-io/io';
import { defaultBootConfig } from '../../lib/turtleos-rootfs/rootfs';
import * as TPM from '../../lib/turtleos-package-manager/packagemanager';
import { VALIDPLATFORMS, writePlatform } from '../../lib/turtleos-platform-tools/platform-tools';

const printHelp = (cmd, usage, desc) => {
    return [
        ['\n'+cmd, 'aqua'],
        [`  :  ${usage}\n`, ''],
        ['  =>  ', 'blue'],
        [desc, '']
    ];
};

const CONFIG_CMDS = {
     //
    // CONFIGURATION TOOLS
    // 
    //terminal
    "term":(TOOLS)=>{
        //info about term tools
        TOOLS.print([
            'col',
            ['(term)\n', 'green'],
            ...printHelp('term-conf-get', '(term-conf-get key echo?)', 'Read a specific options value'),
            ...printHelp('term-conf-list', '(term-conf-list echo?)', 'Lists available options'),
            ...printHelp('term-conf-set', '(term-conf-set key value echo?)', 'Set terminal configuration key'),
        ]);
    },
    "term-conf-set": (TOOLS, key, value, bool=true ) => {
        TOOLS.emit("config-write",{key:key, value:value});
        if(bool) TOOLS.print(`Set ${key} to ${value}`);
    },
    "term-conf-get": (TOOLS, key, bool=true) => {
        let b = TOOLS.emit("config-read", {key: key});
        if(bool) TOOLS.print(b);
        return b;
    },
    "term-conf-list": (TOOLS, bool=true) => {
        let d = TOOLS.emit('config-lsit');
        if(bool) d.forEach(c=>TOOLS.print(c));
        return d;
    },
    //wallpaper
    "wallpaper": (TOOLS) => {
        //Info about wallpaper tools
        TOOLS.print([
            'col',
            ['(wallpaper)','green'],
            ...printHelp('wallpaper-list', '(wallpaper-list echo?)', 'Lists all wallpapers'),
            ...printHelp('wallpaper-set', '(wallpaper-set id)', 'Sets wallpaper to id')
        ]);
    },
    "wallpaper-list": (TOOLS, bool=true) => {
        if(bool) {
        Object.values(WALLPAPERS).forEach(wp=>{
            TOOLS.print(`${wp.id}: ${wp.title}@${wp.author.name}`);
        });
        }
        return Object.keys(WALLPAPERS);
    },
    "wallpaper-set": (TOOLS, wallpaper=defaultWallpaper()) => {
        if (isWallpaper(wallpaper)) {
            pm.dispatch({
                type:'wallpaper:change',
                payload: {
                    wallpaper: wallpaper
                }
            });
        } else {
            TOOLS.throw("Not a wallpaper");
        }
    },
    //network
    "network": (TOOLS)=>{
        //info about network tools
        TOOLS.print([
            'col',
            ['(network)','green'],
            ...printHelp('network-get', '(network-get key echo?)', 'Read a specific config value'),
            ...printHelp('network-list', '(network-list echo?)', 'Show available configuration options'),
            ...printHelp('network-set', '(network-set key value)', 'Set network config to value'),
        ]);
    },
    "network-set": (TOOLS, key, value) => {
        let keys = {'network.on':'network', 'network.auto':'autonetwork'};
        if(Object.keys(keys).indexOf(key)===-1) {
            TOOLS.throw("Key invalid");
            return false;
        } else {
            let res = IO(keys[key])[(value)?'on':'off']();
            if(res) {
                //save new state
                let cont = {};
                try {
                    let file = pm.getState().vfs.readFileSync('/buildconf');
                    cont=JSON.parse(file.data.content);
                } catch(e) {
                    cont=defaultBootConfig;
                }
                pm.getState().vfs.writeFileSync('/buildconf',JSON.stringify({
                    ...cont,
                    network: value
                }));
            }
            return res;
        }
    },
    "network-list": (TOOLS, bool=true) => {
        let keys = {'network.on':'network', 'network.auto':'autonetwork'};
        let r = Object.keys(keys);
        if(bool) r.forEach(c=>TOOLS.print(c));
        return r;
    },
    "network-get": (TOOLS, key, bool=true) => {
        let keys = {'network.on':'network', 'network.auto':'autonetwork'};
        if(Object.keys(keys).indexOf(key)===-1) {
            TOOLS.throw("Key invalid");
            return false;
        } else {
            let conf = pm.getState().io;
            let r = conf[keys[key]];
            if(bool) TOOLS.print(`${key}: ${r}`);
            return r;
        }
    },
    //accessibility
    "acsiblty": (TOOLS)=>{
        //show info about acsblty
        TOOLS.print([
            'col',
            ['(acsiblty)','green'],
            ...printHelp('acsiblty-get', '(acsiblty-get key echo?)', 'Reads a config value'),
            ...printHelp('acsiblty-list', '(acsiblty-list echo?)', 'Lists all configuration options'),
            ...printHelp('acsiblty-set', '(acsiblty-set key value echo?)', 'Sets a config value')
        ]);
    },
    "acsiblty-list":(TOOLS, bool=true)=>{
        let r = ['screen.brightness', 'locale.language'];
        if(bool) r.forEach(c=>TOOLS.print(c));
        return r;
    },
    "acsiblty-get": (TOOLS, key, bool=true) => {
        switch(key) {
        case "screen.brightness":
            let r = pm.getState().io.display.brightness;
            if(bool)TOOLS.print(`${key}: ${r}`);
            return r;
        case "locale.language":
            let rl = pm.getState().lang.lang;
            if(bool) TOOLS.print(`${key}: ${rl}`);
            return rl;
            default:
                TOOLS.throw("Key not found");
        }
        return false;
    },
    "acsiblty-set": (TOOLS, key, value, bool=true) => {
        switch(key) {
            case "screen.brightness":
            let res =  IO('brightness').setBrightness({perc:value});
            if (res) {
                //save to disk
                let cont = {};
                try {
                    let file = pm.getState().vfs.readFileSync('/buildconf');
                    cont=JSON.parse(file.data.content);
                } catch(e) {
                    cont=defaultBootConfig;
                }
                pm.getState().vfs.writeFileSync('/buildconf',JSON.stringify({
                    ...cont,
                    brightness: pm.getState().io.display.brightness
                }));
            }
            if(bool)TOOLS.print(`Set ${key} to ${value} [${res}]`);
            return res;
            case "locale.language":
            let resl = pm.getState().lang.setLanguage(value);
            if(resl) {
                let cont = {};
                try {
                    let file = pm.getState().vfs.readFileSync('/buildconf');
                    cont=JSON.parse(file.data.content);
                } catch(e) {
                    cont=defaultBootConfig;
                }
                pm.getState().vfs.writeFileSync('/buildconf',JSON.stringify({
                    ...cont,
                    lang: value
                }));
            }
            if(bool) TOOLS.print(`Set ${key} to ${value} [${resl}]`);
            return resl;
        default:
            TOOLS.throw("Key not found");
        }
        return false;
    },
    //
    // DEVICE Control
    //
    "reboot": (TOOLS)=>{
        TOOLS.print("Rebooting...");
        setTimeout(()=>{
            window.location.reload();
        },500);
    },
    "fullscreen": (TOOLS)=>{
        let screen = document.getElementById("root");
        let ts = ['requestFullscreen', 'webkitRequestFulllscreen', 'msRequestFullscreen'];
        let stat = 0;
        ts.forEach(t=>{
            if(screen[t] && stat===0) {
                screen[t]();
                TOOLS.print("Launching Fullscreen");
                stat++;
            }
        });
        if(stat!==0) return true;
        TOOLS.throw('Fullscreen not supported');
        return false;
    },
    //
    // Activity Manager
    //
    "am": (TOOLS) =>{
        // information about am
        TOOLS.print([
            'col',
            ['(am)','green'],
            ...printHelp('am-intentify', '(am-intentify ...params)', 'Provide params to be generated to JSON Intent'),
            ...printHelp('am-launch', '(am-launch id intent?)', 'Launches app with specified id'),
            ...printHelp('am-packages', '(am-packages)', 'Shows package specific commands'),
            ...printHelp('am-permission', '(am-permission)', 'Shows permisison specific commands')
        ]);
    },
    "am-launch": (TOOLS, id, intent={}) => {
        let platform = pm.getState().platform;
        let am = pm.getState().am;
        let apps = Object.keys(am.listApps());
        if(apps.indexOf(id)===-1) {
            TOOLS.throw(`App with id ${id} not found`);
            return false;
        }
        TOOLS.print(`Launching ${id}`);
        am.am.startApp(platform, id, intent);
        return true;
    },
    "am-intentify": (TOOLS, ...params) => {
        let intent = {};
        params.forEach(p=>{
            let [key, value] = p.split("=");
            intent[key]=value;
        });
        let i = JSON.stringify(intent);
        return i;
    },
    "am-packages": (TOOLS) =>{
        // info about am-packages
        TOOLS.print([
            'col',
            ['(am-packages)','green'],
            ...printHelp('am-packages-list', '(am-packages-list echo?)', 'Lists all installed apps (applayers, platformlayers))'),
            ...printHelp('am-packages-remove', '(am-packages-remove ...ids)', 'Attemps App removal of apps specified in ids'),
        ]);
    },
    "am-packages-list": (TOOLS, bool=true)=>{
        let am = pm.getState().am.am;
        let apps = Object.keys(am.listApps());
        if(bool) apps.forEach(id=>TOOLS.print(id));
        return apps;
    },
    "am-packages-remove":(TOOLS, ...ids) => new Promise((res, rej)=>{
        let status = 0;
        let failed = 0;
        let removed = 0;
        const isDone = () => {
            status++;
            if(status>=ids.length) {
                TOOLS.print([
                    'col',
                    ['==> DONE\n', 'blue'],
                    ['  REMOVED: ', ''],
                    [`${removed}\n`, 'green'],
                    ['  FAILED:  ', ''],
                    [`${failed}\n`,'red']
                ]);
                res();
            }
        };
        if(ids.length<1) isDone();
        ids.forEach(id=>{
            TOOLS.print([
                'col',
                ['--> ', 'blue'],
                [`Removing ${id}:\n`, '']
            ]);
            TPM.uninstallApp(id)
                .then(d=>{
                    TOOLS.print(d);
                    removed++;
                    isDone();
                })
                .catch(e=>{TOOLS.throw(e); failed++; isDone();});
        });
    }),
    "am-permission": (TOOLS) =>{
        //permission info
        TOOLS.print([
            'col',
            ['(am-permisison)','green'],
            ...printHelp('am-permission-list', '(am-permission-list echo?)', 'Lists all kinds of permission'),
            ...printHelp('am-permission-grant', '(am-permission-grant id permission grant? bool?)', 'Grant or ungrant Apppermisisons'),
            ...printHelp('am-permission-granted', '(am-permission-granted appid bool)', 'list permissions granted to an app'),
            ...printHelp('am-permission-requested', '(am-permission-requested appid bool)', 'list permissions requested by an app')
        ]);
    },
    "am-permission-list": (TOOLS)=>{
        //list permission
    },
    "am-permission-granted": (TOOLS, appid, bool=true) => {
        const am = pm.getState().am;
        let perm = am.am.getPermissions(appid);
        if(bool) {
            let out = [];
            Object.entries(perm).forEach(([key, value]) => {
                if(value) out.push(key);
            });
            TOOLS.print(out.join(','));
        }
        return perm;
    },
    "am-permission-grant": (TOOLS, appid, perm, grant=true, bool=true) => {
        // grant ungrant permissiion depending on grant
        const am = pm.getState().am;
        let res = am.am.grantAppPermission(am.am, appid, {
            [perm]: grant
        }, false, {
            platform: pm.getState().platform
        });
        if(bool) TOOLS.print(`${(grant)?'g':'ung'}ranting permission ${perm} ${(res)?'successful':'failed'}`);
        return res;
    },
    "am-permission-requested": (TOOLS, appid, bool=true) => {
        const am = pm.getState().am;
        let perm = am.am.requestedPermissions(appid);
        if(bool) {
            let out = [];
            Object.entries(perm).forEach(([key, value]) => {
                if(value) out.push(key);
            });
            TOOLS.print(out.join(','));
        }
        return perm;
    },
    //
    // PACKAGE MANAGMENT
    //
    "toss": (TOOLS) => {
        //information about toss
        TOOLS.print([
            'col',
            ['(toss)','green'],
            ...printHelp('toss-info', '(toss-info url)', 'Returns .toss manifest'),
            ...printHelp('toss-install', '(toss-install url)', 'Installs .toss package from url'),
        ]);
    },
    "toss-install": (TOOLS, url)=>new Promise(async (res, rej)=>{
        if(url.indexOf('.toss')!==-1) url=url.slice(0,url.indexOf('.toss'));
        TOOLS.print([
            'col',
            [`Installing Package from ${url} \n`, 'yellow']
        ]);
        await TOOLS.run([
            'toss-info',
            url
        ]);
        TPM.installApp(url)
            .then(data=>{
                if(data) {
                    TOOLS.print('Installed successfully');
                } else {
                    TOOLS.throw('Instalation failed');
                }
                res();
            })
            .catch(e=>{
                TOOLS.throw('Installation failed');
                TOOLS.throw(JSON.stringify(e));
                res();
            });
    }),
    "toss-info": (TOOLS, url="")=>new Promise((res, rej) =>{
        if(url.indexOf('.toss')!==-1) url=url.slice(0,url.indexOf('.toss'));
        TPM.fetchApp(url)
            .then(data=>{
                TOOLS.print([
                    'col',
                    [`\nTOSS: ${url}.toss\n`, 'yellow'],
                ]);
                Object.entries(data.manifest).forEach(([key, value]) => {
                    TOOLS.print([
                        "col",
                        [key, "blue"],
                        [' = ', ''],
                        [( typeof value === "object")?JSON.stringify(value):value, ""],
                        ['\n','']
                    ]);
                });
                TOOLS.print('\n');
                res();
            })
            .catch(e=>{
                TOOLS.throw(e);
                console.warn(e);
                res();
            });
    }),
    "install": (TOOLS) => new Promise((resolve, reject)=>{
        const install = pm.getState().install;
        if(!install.detected) {
            TOOLS.throw("Couldn't install to hard disk");
            resolve(false);
        }
        install.data.prompt().then(res=>{
            if(res.outcome==="accepted") {
                //user has installed -> unrender
                pm.dispatch({
                    type:'install:hard-disk',
                    payload: {
                        status:false,
                        data: {
                        }
                    }
                });
                TOOLS.print("Installed");
                resolve(true);
            } else {
                TOOLS.throw("Couldn't install to hard disk, check Permission and try again");
                resolve(false);
            }
        });
    }),
    "platform": (TOOLS) => {
        TOOLS.print([
            'col',
            ['(platform)','green'],
            ...printHelp('platform-get', '(platform-get bool)', 'Returns current platform from buildconf'),
            ...printHelp('platform-list', '(platform-list bool)', 'Lists all available platforms'),
            ...printHelp('platform-set', '(platform-set platform reboot=true)', 'Sets the platform and attempts reboot'),
        ]);
    },
    "platform-get": (TOOLS, bool=true) => {
        const fs = pm.getState().vfs;
        try {
            let d = fs.readFileSync('/buildconf');
            if(d.error) throw new Error(d);
            let cont = JSON.parse(d.data.content);
            let platform = cont.platform;
            if(bool) TOOLS.print(platform);
            return platform;
        } catch(e) {
            TOOLS.throw(e);
            return "";
        }
    },
    "platform-list": (TOOLS, bool=true) => {
        let plats = VALIDPLATFORMS;
        if(bool) TOOLS.print(plats.join(","));
        return plats;
    },
    "platform-set": (TOOLS, platform, bool=true) => {
        try {
            writePlatform(platform);
            TOOLS.print('Wrote config; Attempting reboot');
            TOOLS.run(['reboot']);
            return true;
        } catch(e) {
            TOOLS.throw(e);
            return false;
        }
    }
};

const TURTLE_COMMANDS = {
    ...LISP,
    ...CONFIG_CMDS,
    "help": (TOOLS) =>{
        const colorful = (cmd) => {
            if(Object.keys(LISP).indexOf(cmd)!==-1) return 'blue';
            if(Object.keys(CONFIG_CMDS).indexOf(cmd)!==-1) return 'yellow';
            if(Object.keys(TURTLE_COMMANDS).indexOf(cmd)!==-1) return 'aqua';
            return 'orange';
        };
        TOOLS.print([
            "col",
            ["+--COMMANDS--+", "purple"]
        ]);
        let items = ['col'];
        Object.keys({...TOOLS.custom('commands')}).sort().forEach(c=>items.push(["sym", c, colorful(c)]));
        TOOLS.run(['stylistic-wrap', ...items]);
    },
    "whoami": ( TOOLS ) => {
        TOOLS.print("You're a turtle");
    },
    "toggle": (TOOLS, v)=>{
        return !(["t","True",true].indexOf(v)!==-1);
    },
    "neofetch": ( TOOLS ) => {
        const fs = pm.getState().vfs;
        const internal = pm.getState().internal;
        let doc = {error:false};
        try {
            let fl = fs.readFileSync('/buildconf');
            doc={
                ...doc,
                ...JSON.parse(fl.data.content)
            };
        } catch(e) {
            doc.error=true;
            doc.msg=e;
        }
        let uptime = '';
        let bootcount;
        if(doc.error) {
            doc.bootcount=0;
        }
        uptime=(new Date(internal.uptime + internal.usage)).toISOString().substr(11,8);
        bootcount=doc.bootcount;
        TOOLS.print([
            "col",
            ["\n\n",""],
            ["     ####     ", "red"],   [`    ${TURTLEOS.release.prefix}\n`, ""],
            ["  ##########  ", "blue"],  [`    ${TURTLEOS.release.name}\n`,""],
            [" ############ ", "green"], [`    Version: ${TURTLEOS.release.version.join('.')}\n`,""],
            [" ############ ", "aqua"],  [`    Bootcount: ${bootcount}\n`,""],
            ["   #######    ", "purple"],[`    Uptime: ${uptime}h\n`,""],
            ["\n\n",""]
            ]);
    },
    "clear": (TOOLS) => {
        TOOLS.emit("clear",{});
    },
    "cclear": (TOOLS) =>{
        TOOLS.run(["progn", ['clear'], ["neofetch"]]);
    },
    //
    // Stylistic Output
    //
    "stylistic": (TOOLS) => {
        
    },
    "stylistic-wrap": (TOOLS, ...text)=>{
        TOOLS.print(text, {
            style: {
                display:'grid',
                gridTemplateColumns:'max-content',
                width:'min-content'
            },
            child: {
                style: {
                    flex: 1
                }
            },
            parent:{
                style:{
                    display:'flex',
                    flexDirection:'row',
                    flexWrap:'wrap'
                }
            }
        });
    },
    //
    // FS TOOLS
    //
    "pwd": (TOOLS)=>{
        TOOLS.print(TOOLS.global.PWD);
    },
    "cd": (TOOLS, path)=>{

        const checkMy = (arr, sym) => {
            if ( arr.indexOf(sym) !== -1) {
                if(sym==='..') {
                    arr.splice(arr.indexOf(sym)-1,2);
                } else {
                    let ind = arr.indexOf(sym);
                    arr.splice(ind,1);
                }
                return checkMy(arr, sym);
            } else {
                return arr;
            }
        };
        let p = path.replaceAll('~','home').split("/");
        checkMy(p, '..');
        checkMy(p, '.');
        checkMy(p, '~');
        p.unshift('');
        let pp = ('/'+p.join('/')).replaceAll('//','/');
        TOOLS.global.PWD=pp;
    },
    "path": (TOOLS, path, bool=true) => {
        const checkMy = (arr, sym) => {
            if ( arr.indexOf(sym) !== -1) {
                if(sym==='..') {
                    arr.splice(arr.indexOf(sym)-1,2);
                } else {
                    let ind = arr.indexOf(sym);
                    arr.splice(ind,1);
                }
                return checkMy(arr, sym);
            } else {
                return arr;
            }
        };
        let p = path.replaceAll('~','home').split("/");
        checkMy(p, '..');
        checkMy(p, '.');
        checkMy(p, '~');
        let pp = ('/'+p.join('/')).replaceAll('//','/');
        if(bool) TOOLS.print(pp);
        return pp;
    },
    "nom": (TOOLS, ...childs) => {
        return childs;
    }
   
};
const TURTLE_BLACKLIST = [
    ...LISP_BLACKLIST,
    "am-intentify",
    "nom",
];
const TYPES = {
    MESSAGE: "message",
    ERROR: "error",
    INPUT: "input",
    BEGIN: 'begin',
    END: 'end',
    EVENT: 'event',
    CMD: 'cmd'
};

function TSH(data={bool:false, am:{}, tpm:{}}) {

    const GLOB = {
        PWD: '/home'
    };

    let state = GLOB;
    let commands = TURTLE_BLACKLIST;
    let blacklist = TURTLE_BLACKLIST;

    //load custom commands from am
    if(data.bool) {
        try {
            let am = data.am;
            let tpm = data.tpm;
            commands = {...TURTLE_COMMANDS};
            state = {...GLOB};
            blacklist = [...TURTLE_BLACKLIST];
            let apps = am.am.listApps();
            Object.values(apps).forEach(data=>{
                if(data.manifest) {
                let manifest = data.manifest;
                let icon = data.icon;
                let app = data.app;
                let appdk = data.appdk;
                if(manifest.type==='shell') {
                let ia = tpm.initialiseApp({...manifest, icon:icon}, app, appdk);
                if(!ia.error) {
                commands={...commands,
                      ...ia.app.shell.COMMANDS};
                state={...state,
                     ...ia.app.shell.STATE};
                blacklist=[...blacklist,
                           ...ia.app.shell.BLACKLIST];
                    }
                }}
            });
    }catch(e) {
        //dont load custom commands for now
        console.log(e);
    }}

    const execute = ( lisp, stream ) => {
        stream.push({type:TYPES.BEGIN});
        const jli = JLI({
            STATE: state,
            COMMANDS: commands,
            BLACKLIST: blacklist,
            DEFAULTS: {
                output: (msg="",conf={style:{},
                                      child:{style:{}},
                                      parent:{style:{}}
})=>stream.push({
                    type: TYPES.MESSAGE,
                    cont: msg,
                    conf: conf
                }),
                input: (msg="")=>{
                    stream.push({
                        type: TYPES.INPUT,
                        cont: `Attempting user input: \n ${msg}`
                    });
                    return prompt(msg);
                },
                error: (err="")=>stream.push({
                    type: TYPES.ERROR,
                    cont: err
                }),
                event: (event="", data={})=>stream.push({
                    type: TYPES.EVENT,
                    event: event,
                    data: data
                }),
                custom: (type, ...v)=>{
                    switch(type) {
                    case "commands":
                        return commands;
                    default:
                    return false;
                    };}
            }
        });
        // begin execution
        return Promise.resolve(jli.runLisp(lisp)).then(()=>{
        setTimeout(()=>{
            stream.push({type:TYPES.END});
        },500);
        });
    };

    return {
        execute
    };
}
export {
    TSH,
    TURTLE_COMMANDS,
    TYPES
}
