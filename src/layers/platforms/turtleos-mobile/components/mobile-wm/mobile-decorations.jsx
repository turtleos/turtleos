import React from 'react';
import { useSelector } from 'react-redux';

function Clock({style={}}) {
    const time = useSelector(state=>state.internal.clock);
    return(
        <div style={{
            fontFamily:'monospace',
            color:'#ebdbb2',
            margin:'0 1rem',
            ...style
        }}>
          {(time.getHours()<10)?'0':''}{time.getHours()}:{(time.getMinutes()<10)?'0':''}{time.getMinutes()}:{(time.getSeconds()<10)?'0':''}{time.getSeconds()}
        </div>
    );
}

export {
    Clock
}
