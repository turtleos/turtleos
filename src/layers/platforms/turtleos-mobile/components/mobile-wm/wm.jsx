import React from 'react';
import * as Feather from 'react-feather';
import { useSelector } from 'react-redux';

import { MobileRecents } from '../mobile-recents/recents';
import { MobileAppMenu } from '../mobile-appmenu/appmenu';
import { MobileViewport } from './mobile-viewport';
import { Clock } from './mobile-decorations';

function MobileWM() {
    const am = useSelector( state => state.am );
    const wallpaper = useSelector( state => state.wallpaper );
    const io = useSelector( state => state.io );

    const path = {
        recents: 'appmenu',
        appmenu: 'recents',
        appview: 'recents'
    };
    const [mode, setMode] = React.useState('recents');

    const followPath = () => {
        setMode(path[mode]);
    };

    function refreshView(windows) {
        let tmp = [];
        for(let value of Object.values(windows)) {
            if (!value) continue;
            tmp.push(value);
        }
        return tmp;
    }

    return (
        <div style={{
            ...wallpaper.bg,
            height: '100%',
            width:'100%',
            filter:`brightness(${io.display.brightness}%)`
        }} className="wallpaper-fix">

          {/*Statusbar*/}
          <div className="statusbar" style={{
              height:'1.6rem',
              width:'100%',
              position:'fixed',
              top:0,
              left:0,
              background: '#1d2021',
              display:'flex',
              flexDirection:'row',
              justifyContent:'right',
              alignItems:'center',
          }}>
            <div></div>
            <Clock/>
          </div>

          {/*Viewport for windows*/}
          <div className="viewport" id='desktop-viewport' style={{
              height:'calc(100% - 4rem)',
              width:'100%',
              position:'fixed',
              top:'1.6rem',
              left:0,
          }}>
            {/*Offscreen rendering*/}
            <div className="offset" style={{
                height:'100%',
                width:'100%',
                position:'absolute',
                top:0,
                left:0,
            }}>
              <MobileViewport shown={mode==='appview'} windows={refreshView(am.windows)}/>
              <MobileAppMenu onClose={()=>setMode('appview')} shown={mode==='appmenu'}/>
              <MobileRecents onClose={()=>setMode('appview')} shown={mode==='recents'}/>
            </div>

          </div>

          {/*Navigationbar*/}
          <div className="navbar" style={{
              height:'calc(2rem - 0.4rem)',
              width:'100%',
              position:'fixed',
              bottom:0,
              left:0,
              background: '#1d2021',
              display:'flex',
              flexDirection:'row',
              alignItems:'center',
              justifyContent:'center',
              padding:'0.4rem'
          }} onClick={()=>{
              //Navbar click
              followPath();
          }}>
            <Feather.ChevronUp color='#fbf1c7' size={48} ></Feather.ChevronUp>
          </div>

         
        </div>
    );
}
export {
    MobileWM
}
