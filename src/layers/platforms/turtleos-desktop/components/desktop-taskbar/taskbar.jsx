import React from 'react';

import turtleoutline from '../../../../../assets/turtle-assets/turtle-outline.png';
import turtlefilled from '../../../../../assets/turtle-assets/turtle-filled.png';

import { useSelector } from 'react-redux';

import { Scrollbars } from 'react-custom-scrollbars';

import * as TPM from '../../../../../lib/turtleos-package-manager/packagemanager';

/*
Menu Widget
 */
function TurtleIcon({onClick}) {
    const [ hover, setHovered ] = React.useState(false);
    return (
        <div style={{
            height:'3rem',
            width:'3rem',
            display:'flex',
            flexFlow:'row nowrap',
        }} onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)} onClick={onClick}>
          <img alt="icon1" src={turtleoutline} style={{
              width:'100%',
              height:'3rem',
              flex:'none'
          }}/>
          <img alt="icon2" src={turtlefilled} style={{
              width:'100%',
              height:'3rem',
              flex:'none',
              marginLeft:'-100%',
              opacity:(hover)?1:0,
              transition:'opacity .3s'
          }}/>


        </div>
    );
}

function AppMenuItem({ icon, name, appid, onClick}) {
    const [ hovered, setHovered ] = React.useState(false);
    return (
        <div key={appid} style={{
            background: (hovered)?'rgba(29, 32, 33, 0.8)':'transparent',
            transition: 'background .3s',
            padding:'0.4rem',
            width:'calc(100% - 0.8rem)',
            height:'3rem',
            display:'flex',
            flexDirection:'row'
        }} onClick={onClick} onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)}>
          <img alt="" src={icon} style={{
              height:'calc(100% - 0.4rem)',
              filter:'brightness(110%)',
              padding:'0.2rem'
          }}/>
          <div style={{
              color:'white',
              height:'100%',
              display:'flex',
              alignItems:'center',
              justifyContent:'center'
          }}>
            {name}
          </div>
        </div>
    );
}

function AppMenu(props) {
    const am = useSelector(state => state.am);

    if(props.hidden) {
        return (
            <div> </div>
        );
    }

    let apps = [];
    for(let [appid, app] of Object.entries(am.am.listApps())) {
        if(appid!==app.id) continue;
       
        apps.push(<AppMenuItem key={app.id} appid={app.id} icon={app.icon} name={app.name} onClick={()=>{
            props.close();
            if(Object.keys(am.windows).indexOf(appid)!==-1) {
                am.am.getAppById(appid).dispatch(new Event('focus'));
                if(am.hidden[appid]) am.am.toggleHideApp(appid);
            } else {
                am.am.startApp('desktop',app.id);
            }
        }}/>);
    }

    return (
        <div style={{
            position:'fixed',
            bottom:'calc(3rem + 2 * 0.4rem + 0.2rem)',
            left:'0.2rem',
            minHeight:'300px',
            height:'50vh',
            minWidth:'200px',
            width:'30vw',
            maxWidth:'60vw',
            background:'rgba(29, 32, 33, 0.8)',
            zIndex:5000000,
            borderRadius:'15px',
            overflow:'hidden'
        }}>
        <Scrollbars>
        {apps}
        </Scrollbars>
        </div>
    );
};


function MenuWidget(props) {
    const [ menuShown, setMenuShown ] = React.useState(false);


    return (
        <div style={{
            width:'3rem',
            height:'3rem',
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            marginLeft:'0.4rem'
        }}>
          <AppMenu close={()=>{setMenuShown(false);}} hidden={!menuShown}/>
          <div style={{
              width:'100%',
              height:'100%',
              top:0,
              left:0,
              position:'fixed',
              display:(menuShown)?'block':'none'
          }} onClick={()=>{setMenuShown(false);}}>
          </div>
          <TurtleIcon onClick={()=>{
              let tmp = menuShown;
              setMenuShown(!tmp);
          }}/>
        </div>
    );
}

/*
Running Apps Dock
 */
function AppIndicator({appid, am}) {

    const [ hovered, setHovered ] = React.useState(false);

    let app = am.am.getAppById(appid);
    return (
        <div onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)} onClick={()=>{
            //left/single click
            am.am.getAppById(appid).dispatch(new Event('focus'));
            if(am.hidden[appid]) am.am.toggleHideApp(appid);
        }} onContextMenu={(e)=>{
            //right/long click
            e.preventDefault();
            if(am.hidden[appid]) am.am.getAppById(appid).dispatch(new Event('focus'));
            am.am.toggleHideApp(appid);
        }} style={{
            margin:'0px 0.2rem',
                  }}>
          <img alt={app.name} src={app.icon} style={{
              height:'calc(2rem + 0.8rem - 2 * 0.4rem)',
              marginTop:'-0.4rem',
              boxShadow:`inset 0px -15px 10px -10px ${(am.am.appIsHidden(appid))?'transparent':'rgba(69, 133, 136, 0.6)'}`,
              //backgroundImage:'linear-gradient(0deg, transparent 80%, red 10%, transparent 10%)',
              padding:'0.4rem',
              filter: `brightness(${(hovered)?150:100}%)`,
              background: (hovered)?'rgba(29, 32, 33, 0.8)':'transparent',
              transition:'filter .2s, background .2s, boxShadow .2s',
          }}/>
        </div>
           );
}
function RecentsDock(props) {
    const am = useSelector(state => state.am);
    let running = [];
    for(let win of Object.keys(am.windows)) {
        running.push(<AppIndicator am={am} key={win} appid={win}/>);
    }
    return (
        <div style={{
            height:'100%',
            width:'auto',
            display:'flex',
            flexDirection:'row'
        }}>
          {running}
        </div>
    );
}

/*
Seperator
 */
function Seperator(props) {
    return (
        <div style={{
            margin:'0.3rem 0.4rem',
            position:'relative'
        }}>
        </div>
    );
}


/*
Render Taskbar
 */

function DesktopTaskbar(props) {
    return (
        <div style={{
            width: 'calc(100% - 1rem - 2 * 0.4rem)',
            borderRadius:'15px',
            height: '2rem',
            background: 'rgba(18, 21, 22, 0.89)',
            position: 'fixed',
            bottom:'0.5rem',
            left:'0.5rem',
            padding:'0.4rem',
            display:'flex',
            flexDirection:'row',
            alignItems:'center'
        }}>
          <MenuWidget/>
          <Seperator/>
          <RecentsDock/>
        </div>
    );
}

export {
    DesktopTaskbar
}
