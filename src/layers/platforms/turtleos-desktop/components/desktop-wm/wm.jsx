import React from 'react';

import { useSelector } from 'react-redux';

import { DesktopTaskbar } from '../desktop-taskbar/taskbar';

import { DecoratedDesktop } from './desktop-decorations';

function DesktopWM() {
    const am = useSelector( state => state.am );
    const wallpaper = useSelector( state => state.wallpaper );
    const io = useSelector( state => state.io );

    function refreshView(windows) {
        let tmp = [];
        for(let value of Object.values(windows)) {
            if (!value) continue;
            tmp.push(value);
        }
        return tmp;
    }

    return (
        <div style={{
            ...wallpaper.bg,
            height: '100%',
            width:'100%',
            filter:`brightness(${io.display.brightness}%)`
        }} className="wallpaper-fix">

          {/*Viewport for windows*/}
          <div className="viewport" id='desktop-viewport' style={{
              height:'calc(100% - 3rem)',
              width:'100%',
              position:'fixed',
              top:0,
              left:0,
          }}>
            {refreshView(am.windows)}
            <DecoratedDesktop/>
          </div>

          {/*Taskbar*/}
          <DesktopTaskbar />


        </div>
    );
}
export {
    DesktopWM
}
