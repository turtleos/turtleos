import React from 'react';

import * as Feather from 'react-feather';

import Draggable from 'react-draggable';

import { getWindowDimensions } from '../../../../../lib/turtleos-platform-tools/platform-tools';

import { useSelector } from 'react-redux';

import { Resizable } from 're-resizable';

function DesktopWindow(props) {
    const vim = useSelector(state=>state.vim);
    const am = useSelector(state=>state.am);
    const [focus, setFocus] = React.useState(vim.get());

    props.bridge.addEventListener('focus',()=>{
        let num = vim.get();
        num++;
        vim.set(num);
        setFocus(num);
    });

    const [ render, setRender ] = React.useState(0);

    const [ width, setWidth ] = React.useState(getWindowDimensions().width);
    const [ height, setHeight ] = React.useState(getWindowDimensions().height);

    const [ histWidth, setHistWidth ] = React.useState(getWindowDimensions().width*0.75);
    const [ histHeight, setHistHeight] = React.useState(getWindowDimensions().height*0.75);

    if(render === 0) {
        let oldHeight = histHeight;
        let oldWidth= histWidth;
        setHistHeight(height);
        setHistWidth(width);
        setHeight(oldHeight);
        setWidth(oldWidth);

        setRender(1);
    }

    return (
        <Draggable
            handle='.handle1'
           bounds={{
                left:0,
                top:-height/2,
                right: document.getElementById('desktop-viewport').offsetWidth - width / 2,
                bottom: document.getElementById('desktop-viewport').offsetHeight - height / 2 - 13
            }}
            onMouseDown={()=>{
                let num = vim.get();
                num++;
                vim.set(num);
                setFocus(num);
            }}
          >
            <Resizable
              defaultSize={{
                  width: getWindowDimensions().width/0.5,
                  height: getWindowDimensions().height/0.5
              }}
              bounds={'window'}
              size={{
                  width: width,
                  height: height
              }}
              onResizeStop={(e, dir, ref, d) => {
                  setWidth(width + d.width);
                  setHeight(height + d.height);
              }}
              maxWidth={getWindowDimensions().width}
              maxHeight={getWindowDimensions().height}
              minWidth={getWindowDimensions().width/10}
              minHeight={getWindowDimensions().height/10}
             /* Disable some corners enable={{
                  bottomRight:true,
                  right:true,
                  bottom:true
              }}*/ style={{
                  zIndex:focus,
                  display:(am.hidden[props.appid])?'none':'block',
                  position:'fixed',
                  transition:'width .1s, height .1s'
              }}>
              <div style={{
                  width:'100%',
                  height:'100%',
                  borderRadius: '15px',
                  overflow:'hidden',
                  position: 'absolute',
                  boxShadow: '0px 0px 17.5px -10px black',
                  zIndex:focus,
              }}>
            {/*Viewport*/}
              <div className="window-viewport" style={{
                  width: 'calc( 100% - 3rem)',
                  height: '100%',
                  background: '#504945',
                  position: 'absolute',
                  left:'3rem',
                  top:0
                  }}>
                {props.view}
            </div>
            {/*Sidebar*/}
                <WindowSidebar changeDimens={()=>{
                    if(width>getWindowDimensions().width-10&&height>getWindowDimensions().height-10){
                        //Is fullscreen minimize
                        setHeight(histHeight);
                        setWidth(histWidth);
                    } else {
                        //minimized -> maximize
                        setHistHeight(height);
                        setHistWidth(width);
                        setHeight(getWindowDimensions().height);
                        setWidth(getWindowDimensions().width);
                    }
                }} dimens={[width, height]} appid={props.appid} focused={focus===vim.get()}/>
        </div>
        </Resizable>
          </Draggable>
    );
}

function WindowSidebar(props) {
    const am = useSelector(state=>state.am);
    const ICONSTYLE = {
        color:'#fbf1c7',
        size: 32,
    };
    const BasicStyle = {
        height: '3rem',
        width:'100%',
    };
    return (
        <div style={{
            width:'3rem',
            height:'100%',
            background: '#282828',
            top:0,
            left:0,
            //zIndex:-1,
            display:'flex',
            flexDirection:'column',
            alignItems: 'left',
            justifyContent:'left',
            position:'relative'
        }} className="window-sidebar handle">
          <WindowSidebarButton icon={
              <Feather.X {...ICONSTYLE} style={{
                  ...BasicStyle,
              }}/>
          } click={()=>{
              am.am.closeApp(props.appid);
          }}/>
          <WindowSidebarButton icon={
              (props.dimens[0]>=getWindowDimensions().width-10 && props.dimens[1]>=getWindowDimensions().height-10)?<Feather.Minimize2 {...ICONSTYLE} style={{
                  ...BasicStyle
              }}/>:<Feather.Maximize2 {...ICONSTYLE} style={{
                  ...BasicStyle
              }}/>
          } click={()=>{
              //toggle fullscreen / mini
              if (props.dimens[0]>=getWindowDimensions().width-10 && props.dimens[1]>=getWindowDimensions().height-10) {
                  //currently in fullscreen
              } else {
                  
              }
              props.changeDimens();
          }}/>
          <WindowSidebarButton icon={
              <Feather.Minus {...ICONSTYLE} style={{
                  ...BasicStyle,
              }}/>
          } click={()=>{
              am.am.toggleHideApp(props.appid);
          }}/>
          <div className="handle1" style={{
              width:'100%',
              height:'100%'
          }}>
            
          </div>

        </div>
    );
}

function WindowSidebarButton({animate=true,style={},icon, click=()=>{}}) {
    const [brightness, setBrightness] = React.useState(100);
    return (
        <div style={{
            ...style,
            width:'2.2rem',
            padding: '0.4rem',
            background: '#282828',
            filter: `brightness(${brightness}%)`,
            transition: 'filter .5s',
        }} onMouseEnter={()=>{
            if(!animate) return;
            setBrightness(90);
        }} onMouseLeave={()=>{
            if(!animate) return;
            setBrightness(100);
        }} onClick={click}>
          {icon}
        </div>
    );
}

export {
    DesktopWindow
}
