import React from 'react';

import { useSelector, useDispatch } from 'react-redux';

import { TurtleAnimation } from '../../assets/turtle-boot-animation/turtle';

import { usePlatform, detectPlatform, Platform } from '../../lib/turtleos-platform-tools/platform-tools.js';

import { defaultBootConfig } from '../../lib/turtleos-rootfs/rootfs.js';

import { TurtleASCII } from '../../assets/turtle-ascii/art.js';

import { APPLAYERS } from '../applayer/applayers';

import { isWallpaper, defaultWallpaper } from '../../assets/turtle-wallpaper-collection/wallpapers.js';

import * as TPM from '../../lib/turtleos-package-manager/packagemanager';

import { IO } from '../../lib/turtleos-io/io';
import { pm } from '../turtleos-pm/pm';

var BOOTDELAY = 2000;

let s = 0;
let status=0;

function BootSequence1() {
    const vfs = useSelector(state=>state.vfs);
    const wallpaper = useSelector( state => state.wallpaper );
    const am = useSelector(state => state.am);
    const lang = useSelector(state => state.lang);

    const [opacity, setOpacity] = React.useState(1);
    const dispatch = useDispatch();
    let platform = detectPlatform();
    let lng = lang.default;
    let wp = defaultWallpaper;
    let uptime = 0;
    let defaultConf = JSON.parse(defaultBootConfig.content);
    let bright = defaultConf.brightness;
    let autonetw = defaultConf.autonetwork;
    let network = defaultConf.network;
    let config = vfs.readFileSync('/buildconf').data.content;
    try {
        config = JSON.parse(config);
    } catch(e) {
        console.log(`ERROR: An Error occured during boot ${e.toString()}, attempting boot with default config`);
        config = JSON.parse(defaultBootConfig.content);
    }
    if( config.platform!==platform && config.platform!=='auto' ) {
        platform = config.platform;
    }
    //fallback config
    if( !config.platform ) platform = detectPlatform();
    if( !!config.lang ) lng=config.lang;
    if ( !!config.brightness ) bright=config.brightness;
    if ( !!config.autonetwork ) autonetw=config.autonetwork;
    if( !!config.network ) network=config.network;
    if (!navigator.onLine) network=false;
    if (!!config.uptime) uptime=config.uptime;
    //apply walllpaper
    if( isWallpaper(config.wallpaper)) wp=config.wallpaper;

    //Initialise Defaults
    const [plat] = usePlatform(platform);

    //Insider 1 && Tab check
    if(status===0) {
        IO('autonetwork')[(autonetw)?'on':'off']();
        IO('network')[(network)?'on':'off']();

        //online/offline listeners
        window.addEventListener('online',()=>{
            if(pm.getState().io.autonetwork) {
                IO('network').on();
            }
        });
        window.addEventListener('offline',()=>{
            IO('network').off();
        });

        //set language
        lang.setLanguage(lng);
        status++;

        //set brightness
        IO('brightness').setBrightness({perc:bright});

        dispatch({
            type: 'wallpaper:change',
            payload: {
                wallpaper: wp
            }
        });

        dispatch({
            type:'internal:usage',
            payload: {
                usage: uptime
            }
        });

        let attemptshutdown = window.addEventListener('beforeunload', (ev) => {
            ev.preventDefault();
            alert(lang.translate('dialog.shutdown.attemptshutdown'));
        });
        let doshutdown = window.addEventListener('unload', (ev) => {
            ev.preventDefault();
            if(pm.getState().vfs==="reset") {
                localStorage.removeItem('turtleos-storage');
                return;
            }
            let conf = {};
            try {
                let fl = vfs.readFileSync('/buildconf');
                conf=JSON.parse(fl.data.content);
            }catch(e) {
                console.log(e);
                conf=config;
            }
            vfs.writeFileSync('/buildconf',JSON.stringify(Object.assign(conf,{
                uptime: pm.getState().internal.usage + pm.getState().internal.uptime
            })));
            localStorage.setItem('turtleos-storage',JSON.stringify(vfs.export()));
            alert(lang.translate('dialog.shutdown.doshutdown'));
        });

        //console.clear();
    console.log(`
${TurtleASCII}
${lang.translate('console.subtitle')}
`);


        //Increase Bootcount

        vfs.writeFileSync(
            '/buildconf',
            JSON.stringify(Object.assign(config, {
                bootcount: config.bootcount+1,
            }))
        );

        window.addEventListener('beforeinstallprompt',(e={})=>{
            //installable detected
            e.preventDefault();
            dispatch({
                type:'install:hard-disk',
                payload: {
                    status:true,
                    data: e
                }
            });
        });

        /*check if other tabs are opened*/
        localStorage.openpages = Date.now();
        window.addEventListener('storage', (e)=>{
            if(e.key==='openpages') {
                localStorage.page_available= Date.now();
            }
            if(e.key==='page_available') {
                //found other turtleos tabs
                window.removeEventListener('beforeunload',attemptshutdown);
                window.removeEventListener('unload',doshutdown);

                //decrease Bootcount -> not really booting

                vfs.writeFileSync(
                    '/buildconf',
                    JSON.stringify(Object.assign(config, {
                        bootcount: config.bootcount-1
                    }))
                );

                alert(lang.translate('error.othertabopened'));
                window.location.reload();
            }
        }, false);
   // }
    //load applayers
    for (let layer of APPLAYERS) {
        am.am.addApp(layer);
    };
        // load packagelayers
        let ipl = TPM.listInstalledApps();
        if(!ipl.error) {
            ipl.data.forEach(({manifest, src, id, icon})=>{
            TPM.addAppToApps(
                manifest,
                src,
                icon
            );
            });
        }
    }
    if(plat!=='auto' && s===0) {
        s++;
        //am.am.startApp(plat,'icu.ccw.turtleos.carlon');
    }

    if(plat==="auto") {
        console.log(lang.translate('console.somethingisoff'));
    } else {
        //attempt boot
        dispatch({
            type: 'tsh:reload',
            payload: {
                am: am,
                tpm: TPM
            }
        });
        setTimeout(()=>{

        dispatch({
            type: 'view:change',
            payload: {
                view: Platform(plat).getBootView()
            }
        });

        }, BOOTDELAY);
        setTimeout(()=>{
            setOpacity(0);
        }, BOOTDELAY-200);
    }
    //internal clock
    React.useEffect(()=>{
        setInterval(()=>{
            dispatch({
                type: 'internal:clock',
                payload: {
                    date: new Date()
                }
            });
        },1000);
    },[]);

    return (
        <div style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
            width: '100%'
        }}>
          <div className="container" style={{
              height: '100%',
              width:'100%',
              background:'#ffffff',
              positions:'absolute',
              left:0,
              top:0,
              zIndex:20,
              display: 'flex',
              justifyContent:'center',
              alignItems:'center',
              opacity: opacity,
              transition: 'opacity .2s'
          }}>
            <TurtleAnimation />
          </div>
          <div className="bg" style={{
              ...wallpaper.bg,
              width:'100%',
              height:'100%',
              position:'absolute',
              top:0,
              left:0,
          }}></div>
         
        </div>
    );
    }

export {
    BootSequence1
}
