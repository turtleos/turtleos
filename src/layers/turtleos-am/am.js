import { pm } from '../turtleos-pm/pm.jsx';

import { APPDK } from './../../lib/turtleos-appdk/appdk';

import { Platform } from '../../lib/turtleos-platform-tools/platform-tools';

import { PermissionManager, PERMISSION } from './../../lib/turtleos-permissions/permissions';

import * as TPM from './../../lib/turtleos-package-manager/packagemanager';

const FORCE_ENABLED = [
    'icu.ccw.turtleos.settingsmanager',
    'icu.ccw.turtleos.turtleshell'
];

class App {
    constructor(name, id, type, icon, permissions={}, grant={}) {
        let pem = {};
        try {
            let fs = pm.getState().vfs;
            let d = fs.readFileSync(`/usr/apps/${id}/permissions`);
            if(!d.error) {
            let json = JSON.parse(d.data.content);
                pem=json;
            }
        } catch(e){}
        //Permissions
        Object.assign(grant,{
            //Permissions not to grant by default
            // -> They are force disabled
            [PERMISSION.ROOT]:false,
            [PERMISSION.WRITE_STORAGE]:false,
            [PERMISSION.MODIFY_SYSTEM_SETTINGS]:false,
        });
        if(FORCE_ENABLED.indexOf(id)!==-1) {
            //Enable some permissions for settings application
            for(let key of Object.keys(grant)) {
                grant[key]=true;
            }
        }
        Object.entries(pem).forEach(([key, value]) => {
            grant[key]=value;
        });
        this.name=name;
        this.id=id;
        this.type=type;
        this.icon=icon;
        this.appdk=APPDK({
            appid: id,
            permmgr: new PermissionManager({
                requested:permissions,
                granted:grant
            })
        });
        this.requests=permissions;
        this.granted=grant;
    }
    generateAppDK() {
        this.appdk=APPDK({
            appid: this.id,
            permmgr: new PermissionManager({
                requested: this.requests,
                granted: this.granted
            })
        });
    }
    updatePermission(/*PERMISSION[]*/ perms) {
        let fs = pm.getState().vfs;
        //grant permissions
        this.granted=Object.assign(this.granted, perms);
        fs.writeFileSync(`/usr/apps/${this.id}/permissions`, JSON.stringify(this.granted));
        //regenerate appdk
        this.generateAppDK();
    }
    getPermissions() {
        return this.granted;
    }
}

class AppLayer extends App {
    constructor(name = "Salat", id="icu.ccw.salat", { render, icon, requests={}, forceGrant={} }) {
        super(name, id, 'turtleos:layer', icon, requests, forceGrant);
        this.draw=render;
        this.bridge=document.createElement('div');
    }
    render(platform, props) {
        return Platform( platform ).getWindow({
            view:this.draw({props:props,appdk:this.appdk,intent:props.intent,bridge:this.bridge}),
            appid:this.id,
            appname:this.name,
            icon:this.icon,
            bridge: this.bridge,
            appdk: this.appdk
        });
    }
    dispatch(event) {
        this.bridge.dispatchEvent(event);
    }
}
class PackageLayer extends App {
    constructor(manifest, src, icon) {
        let perm = {};
        manifest.permissions.forEach(per=>perm[per]=true);
        super(manifest.name, manifest.id, 'turtleos:package', icon, perm, {});
        this.app=src;
        this.manifest=manifest;
        this.bridge=document.createElement('div');
    }
    render(platform, props) {
        try {
            let d = TPM.initialiseApp(this.manifest, this.app, this.appdk);
            if(d.error) throw new Error('Error initialising App: '+d.msg);
            return Platform( platform ).getWindow({
                view: d.app.render({props: props,
                                 intent: props.intent,
                                 bridge: this.bridge
                                }),
                appid: this.id,
                appname: this.name,
                icon: this.icon,
                bridge: this.bridge,
                appdk: this.appdk
        });

        }catch(e){
            console.log(e);
            return this.appdk.UI.json2react({
                tag:'div',
                children:['Error']
            });
        }
    }
    dispatch(event) {
            this.bridge.dispatchEvent(event);
    }
}

function ActivityManager() {

    this.apps={};
    this.windows={};
    this.hidden={};

    return {
        am:{
            addApp: (app) => {
                this.apps[app.id]=app;
            },
            getPermissions: (id) => {
                return this.apps[id].getPermissions();
            },
            grantAppPermission: (am, id, permissions, autostart, {intent={}, platform='desktop'}) => {
                if(this.apps[id]===undefined) return false;
                //force stop app
                let running = am.isRunning(id);
                if(running) {
                    am.closeApp(id);
                }
                this.apps[id].updatePermission(permissions);
                //autostart if configured to 
                if(autostart && running) am.startApp(platform, id, intent);
                return true;
            },
            requestedPermissions:(id) => {
                return this.apps[id].requests;
            },
            hasPermission: (id, permission) => {
                return this.apps[id].requests[permission];
            },
            startApp: (platform, id, intent={}) => {
                if(!this.apps[id]) return {found:false};
                switch(this.apps[id].type) {
                case "turtleos:layer":
                    this.windows[id]=this.apps[id].render(platform, {intent:intent});
                    break;
                case "turtleos:package":
                    this.windows[id]=this.apps[id].render(platform, {intent:intent});
                    break;
                default:
                    return {
                        found: false
                    };
                }
                pm.dispatch({
                    type: 'am:update',
                    payload: {
                        windows: this.windows
                    }
                });
            },
            isRunning: (id) => {
                if(!!this.windows[id]) return true;
                return false;
            },
            getAppById: (id) => {
                let app = this.apps[id];
                if(!app) return {};
                return app;
            },
            closeApp: (id) => {
                delete this.windows[id];
                pm.dispatch({
                    type: 'am:update',
                    payload: {
                        windows: this.windows
                    }
                });
            },
            hideApp: (id) => {
                pm.dispatch({
                    type:'am:hide-window',
                    payload:{
                        id:id,
                        state:true
                    }
                });

            },
            unhideApp: (id) => {
                pm.dispatch({
                    type:'am:hide-window',
                    payload:{
                        id:id,
                        state:false
                    }
                });

            },
            toggleHideApp: (id) => {
                let state = pm.getState();
                let wmstate = state.am.hidden[id];
                if(wmstate){
                    wmstate=false;
                } else {
                    wmstate=true;
                }
                pm.dispatch({
                    type: 'am:hide-window',
                    payload: {
                        id: id,
                        state: wmstate
                    }
                });
            },
            appIsHidden: (id) => {
                let state = pm.getState();
                if(state.am.hidden[id]) return true;
                return false;
            },
            listApps: () => {
                return this.apps;
            }
        },
        windows: this.windows,
        hidden: this.hidden
    };
};


export {
    AppLayer,
    PackageLayer,
    ActivityManager
}
