import { AppLayer } from '../../turtleos-am/am';
import icon from './icon.png';
import {TRANSLATIONS} from './translations.js';
import {PERMISSION} from '../../../lib/turtleos-permissions/permissions';

import React from 'react';

function SettingsSubmenuItem1({icon, text,}) {
    return (
        <div style={{
            display:'flex',
            flexDirection:'row',
            height:'3rem',
            //width:'100%',
            alignItems:'center'
        }}>
          {/*ICON*/}
          <div style={{
              padding:'0.4rem',
              margin:'0rem 0.8rem',
              width:'calc(3rem - 0.4rem * 2)',
              height:'calc(3rem - 0.4rem * 2)',
              display:'flex',
              justifyContent:'center',
              alignItems:'center'
          }}>
            {icon}
          </div>
          {/*TEXT*/}
          <div style={{
              fontSize:'1.2rem'
          }}>
            {text}
        </div>
        </div>
    );
}
function SettingsSubmenuSeperator1() {
    return (
        <div style={{
            height:'2px',
            width:'95%',
            margin:'0.4rem 2.5%',
            background:'rgba(0,0,0,0.3)'
        }}></div>
    );
}

//
// SUBMENUS
//

// General
function SettingsMenuGeneral({appdk, trans}) {
    const brightness = appdk.System.getState(state=>state.io.display.brightness);
    const lang = appdk.System.getState(state=>state.lang);
    let lang_selector = [];
    for(let lng of lang.languages) {
        lang_selector.push(<option value={lng.toLowerCase()}>{lang.translate('languages.'+lng.toLowerCase())}</option>);
    }
    return (
        <div style={{
            height:'100%',
            width:'100%'
        }}>
          <appdk.UI.Scrollbars>

          {/*BRIGHTNESS*/}
          <div>
            <SettingsSubmenuItem1 text={trans.translate('settings.submenu.common.brightness')} icon={<appdk.UI.ICONS.Feather.Sun size={48}/>}/>
            <div style={{
                display:'flex',
                justifyContent:'center',
                alignItems:'center'
            }}>
              {brightness}%
              <input style={{marginLeft:'0.4rem'}} type="range" min={10} max={100} onChange={(e)=>{
                  appdk.System.Settings.setBrightness({perc:parseInt(e.target.value)});
              }} value={brightness}/>
            </div>
            <SettingsSubmenuSeperator1 />
        </div>
          {/*LANGUAGE*/}
          <div>
            <SettingsSubmenuItem1 text={trans.translate('settings.submenu.common.language')} icon={<appdk.UI.ICONS.Feather.Book size={48}/>}/>
            <select id="settings.language" name="language" value={lang.lang.toLowerCase()} onChange={(e)=>{
                appdk.System.Settings.setLanguage(e.target.value.toLowerCase());
            }}>
              {lang_selector}
            </select>
            <SettingsSubmenuSeperator1 />
        </div>
        </appdk.UI.Scrollbars>
        </div>
    );
}

// Connectivity
function SettingsMenuConnectivity({appdk, trans}) {
    const io = appdk.System.getState(state=>state.io);
    return (
        <div style={{
            height:'100%',
            width:'100%'
        }}>
          <appdk.UI.Scrollbars>
            <div style={{
                display:'flex',
                flexDirection:'row'
            }}>
              <SettingsSubmenuItem1
                text={trans.translate('settings.submenu.connectivity.network')}
                icon={<appdk.UI.ICONS.Feather.Radio size={48}/>}/>
              <appdk.UI.CustomSlider
                style={{marginLeft:'0.4rem'}}
                state={io.network}
                onChange={(st)=>{
                    //toggle network
                    if(!navigator.onLine&&st)st=false;
                    appdk.System.Settings.setNetwork(st);
              }}/>
            </div>
            <SettingsSubmenuSeperator1/>
            <div style={{
                display:'flex',
                flexDirection:'row'
            }}>
              <SettingsSubmenuItem1
                text={trans.translate('settings.submenu.connectivity.autonetwork')}
                icon={<appdk.UI.ICONS.Feather.Link2 size={48}/>}/>
              <appdk.UI.CustomSlider
                style={{
                  marginLeft:'0.4rem'
                }}
                state={io.autonetwork}
                onChange={(st)=>{
                  //toggle autonetwork
                    appdk.System.Settings.setAutoNetwork(st);
              }}/>
            </div>
          </appdk.UI.Scrollbars>
        </div>
    );
}

// Wallpapers
function SettingsMenuWallpapers({appdk, trans}) {
    const wallpaper = appdk.System.getState(state=>state.wallpaper);
    let Scrollbars = appdk.UI.Scrollbars;
    function refreshView(wp) {
        let tmp = [];
        for (let {id, bg, author} of Object.values(appdk.System.getAllWallpapers())) {
            tmp.push(
                <div key={id} style={{
                    ...bg,
                    minWidth:'7rem',
                    //width:`calc( 80% - 6px)`,
                    flex: 1,
                    transition:'.2s',
                    minHeight:'150px',
                    height:'30%',
                    margin:'5%',
                    border:(wp.id===id)?'3px solid #458588':'3px solid white',
                    borderRadius:'15px',
                    justifyContent:'stretch',
                    alignItems:'stretch',
                    position:'relative',
                    overflow:'hidden'
                }} onClick={()=>{
                    appdk.System.Settings.setWallpaper(id);
                }}>
                  <div style={{
                      color: 'white',
                      background:'#212121',
                      position:'absolute',
                      left:0,
                      bottom:0,
                      padding: '0.4rem',
                      borderTopRightRadius:'10px'
                  }} onClick={()=>appdk.AM.startApp('icu.ccw.turtleos.lattus',{
                      url: author.url
                  })}>
                    @{author.name}
                  </div>
                </div>
            );
        }
        return tmp;
    }
    return (
        <div style={{
            width:'100%',
            height:'100%'
        }}>
          <Scrollbars style={{
              margin:'0 5%',
              width:'90%',
              overflow:'hidden',
              display:'flex',
              flexDirection:'column',
              justifyContent:'center',
              alignItems:'center'
          }}>
            <div style={{
                display:'flex',
                flexDirection:'row',
                flexWrap:'wrap'
            }}>
        {refreshView(wallpaper)}
        </div>
          </Scrollbars>
        </div>
    );
}

// Applications
function SettingsMenuApps({appdk, trans}) {
    return (
        <div style={{
            width:'100%',
            height:'100%'
        }}>
          {(appdk.System.getCurrentTime().getMinutes()%2===0)?<appdk.UI.SCREENS.Construction />:<appdk.UI.SCREENS.Sleeping/>}
           </div>
    );
}

// Device
function SettingsMenuDevice({appdk, trans}) {
    const internal = appdk.System.getState(state=>state.internal);
    let time = (new Date(internal.uptime)).toISOString().substr(11,8);
    let usage = (new Date(internal.uptime + internal.usage)).toISOString().substr(11,8);

    let doc = {error:false};
    try {
        let fl = appdk.FS.readFile('/buildconf');
        doc={
            ...doc,
            ...JSON.parse(fl.data.content)
        };
    } catch(e) {
        doc.error=true;
    }

    return (
        <div style={{
            width:'100%',
            height:'100%'
        }}>

          <appdk.UI.Scrollbars>
            <div style={{
                display:'flex',
                flexDirection:'column',
                alignItemns:'center'
            }}>
              <div style={{
                  padding:'0.4rem',
                  display:'flex',
                  width:'100%',
                  alignItems:'center',
                  flexDirection:'column',
                  fontSize:'1.2rem',
                  fontWeight:800
              }}>
                <img alt="TurtleOS Logo" style={{
                    width:'15rem',
                    maxWidth:'50%',
                    marginBottom:'0.4rem',
                }} src={appdk.UI.ICONS.turtle.simple}/>
                TurtleOS SmallWorld
              </div>
              {/*Runtime Stats*/}
              <div>
                {trans.translate('settings.submenu.device.uptime')} {time} {trans.translate('settings.time.hour')}<br/>
                {trans.translate('settings.submenu.device.totalRuntime')} {usage} {trans.translate('settings.time.hour')}<br/>
        {(!doc.error)?`${trans.translate('settings.submenu.device.bootcount')} ${doc.bootcount}`:''}
              </div>
              {/*Options*/}
              <div style={{
                  width:'100%',
                  position:'relative',
                  display:'flex',
                  flexDirection:'column'
              }}>
                <SettingsSubmenuItem1 icon={<appdk.UI.ICONS.Feather.Key/>} text={trans.translate('settings.submenu.device.reset')}/>
                <appdk.UI.CustomSlider onChange={()=>{
                    appdk.FS.reset();
                    window.location.reload();
                }} style={{margin:'0.8rem'}}/>
                <SettingsSubmenuSeperator1 />
              </div>
            </div>
          </appdk.UI.Scrollbars>

        </div>
    );
}

// Info
function SettingsMenuAbout({appdk, trans}) {
    const [showCredits, setShowCredits] = React.useState(false);
    const [showLicense, setShowLicense] = React.useState(false);
    return (
        <div style={{
            height:'100%',
            width:'100%',
        }}>
        <appdk.UI.Scrollbars>
        <div style={{
              display:'flex',
              flexDirection:'column',
              alignItemns:'center'
          }}>
            <div style={{
                padding:'0.4rem',
                display:'flex',
                alignItems:'center',
                flexDirection:'column'
            }}>
              <img alt="TurtleOS Logo" style={{
                  width:'15rem',
                  maxWidth:'50%',
                  marginBottom:'0.4rem',
                  fontSize:'1.2rem'
              }} src={appdk.UI.ICONS.turtle.simple}/>
              TurtleOS
            </div>
            {/*Credit Section*/}
          <div onClick={()=>setShowCredits(!showCredits)}>
            <div style={{
                display:'flex',

            }}>
              <SettingsSubmenuItem1
                text={trans.translate('settings.submenu.about.credits')}
                icon={<appdk.UI.ICONS.Feather.AtSign size={48}/>}/>
            </div>
              <div style={{
                  width:'100%',
                  height:'auto',
                  position:(showCredits)?'relative':'absolute',
                  transform:`scaleY(${(showCredits)?'1':'0'})`,
                  transition:'transform .5s',
                  overflow:'hidden'
              }}>
                {Object.values(appdk.System.getAllWallpapers()).map((wallpaper)=>{
            return (
                <div style={{
                    padding:'0.4rem',
                    borderRadius:'10px',
                    display:'flex',
                    flexDirection:'column',
                    boxShadow:'0px 0px 3px rgba(0,0,0,0.15)',
                    margin:'0.8rem 0.6rem'
                }} onClick={()=>appdk.AM.startApp('icu.ccw.turtleos.lattus',{url:wallpaper.url})}>
                  <div onClick={()=>appdk.AM.startApp('icu.ccw.turtleos.lattus',{url:wallpaper.url})} style={{
                      fontWeight:600,
                      fontSize:'1.2rem'
                  }}>
                    {wallpaper.title} {trans.translate('settings.submenu.about.authored')} {wallpaper.author.name}
                  </div>
                  <div>
                    {wallpaper.url}
                  </div>
                </div>
            );
        })}
              </div>
              <SettingsSubmenuSeperator1/>
          </div>
        {/*License Section*/}
        <div onClick={()=>setShowLicense(!showLicense)}>
        <div style={{
            display:'flex',

        }}>
        <SettingsSubmenuItem1
        text={trans.translate('settings.submenu.about.license')}
        icon={<appdk.UI.ICONS.Feather.Copy size={48}/>}/>
        </div>
        <div style={{
            width:'100%',
            height:'auto',
            position:(showLicense)?'relative':'absolute',
            transform:`scaleY(${(showLicense)?'1':'0'})`,
            transition:'transform .5s',
            overflow:'hidden'
        }}>
          {appdk.System.getState(state=>state.lang).translate('system.license')}
        </div>
        <SettingsSubmenuSeperator1/>
        </div>
        </div>
        </appdk.UI.Scrollbars>
        </div>
    );
}

//MENU UI
const SettingsMenuItemColor = {
    common:'#98971a',
    connectivity:'#cc241d',
    about:'#458588',
    device:'#d79921',
    wallpaper:'#689d6a',
    apps:'#b16286'
};
function SettingsMenuItem({icon, text, color, onClick=()=>{}}) {
    const [hover, setHover] = React.useState(false);
    return (
        <div style={{
            display:'flex',
            flexDirection:'row',
            width:'100%',
            height:'3rem',
            background:(hover)?'#eeeeee':'#ffffff',
            alignItems:'center',
            transition:'background .3s'
        }} onMouseEnter={()=>setHover(true)} onMouseLeave={()=>setHover(false)} onClick={onClick}>
          <div style={{
              width:'calc(3rem - 0.4rem * 2)',
              height:'calc(3rem - 0.4rem * 2)',
              color: color,
              display:'flex',
              alignItems:'center',
              justifyContent:'center',
              margin:'0.4rem 0.8rem',
              marginLeft:(hover)?'1.5rem':'0.8rem',
              transition:'margin-left .2s'
          }}>
            {/*Icon goes here*/}
            {icon}
          </div>
          <div style={{
              fontSize:'1.4rem',
              display:'flex',
              flexDirection:'column',
              alignItems:'center',
              justifyContent:'center'
          }}>
            {/*Text*/}
            {text}
          </div>
        </div>
    );
}

function SettingsMenu({istargeted, menuid, child, title, goback, appdk}) {
    const [backHovered, setBackHovered] = React.useState(false);
    return (
        <div key={menuid} style={{
            width:'100%',
            height:'100%',
            position:'absolute',
            top:0,
            left:(istargeted)?'-100%':'0',
            transition:'left .2s',
            background:'#fff'
        }}>
          <div style={{
              position:'absolute',
              top:0,
              zIndex:1,
              left:0,
              width:'2rem',
              height:'2rem',
              color:'#282828',
              background:(backHovered)?'#d5c4a1':'#ebdbb2',
              transition:'background .2s',
              borderRadius:'10px',
              margin:'0.4rem',
              padding:'0.4rem'
          }} onMouseEnter={()=>{setBackHovered(true);}} onMouseLeave={()=>{setBackHovered(false);}} onClick={()=>{goback();}}>
        <appdk.UI.ICONS.Feather.ChevronLeft size={48} style={{width:'2rem',height:'2rem'}}/>
          </div>
          <div style={{
              top:0,
              zIndex:1,
              left:'calc(2rem + 0.4rem * 2 + 0.4rem * 1)',
              height:'2rem',
              padding:'0.4rem 0.8rem',
              margin:'0.4rem',
              maxWidth:'100%',
              position:'absolute',
              color:'#282828',
              background:'#ebdbb2',
              borderRadius:'10px',
              lineHeight:'2rem',
              textAlign:'center'
          }}>
            {title}
          </div>
          <div style={{
              height:'calc(100% - 2rem - 0.4rem * 4)',
              width:'100%',
              position:'absolute',
              bottom:0,
              left:0,
          }}>
            {child}
          </div>
        </div>
    );
}

function settingsmenuitems(appdk) {
    return {
    common: {
        item: {
            icon: <appdk.UI.ICONS.Feather.Square size={48}/>,
            text: 'settings.menu.common'
        },
        menu: (appdk, trans)=>{
            return (
                <SettingsMenuGeneral appdk={appdk} trans={trans}/>
            );
        }
    },
    connectivity: {
        item: {
            icon: <appdk.UI.ICONS.Feather.Radio size={48}/>,
            text: 'settings.menu.connectivity'
        },
        menu: (appdk, trans)=>{
            return (
                <SettingsMenuConnectivity appdk={appdk} trans={trans}/>
            );
        }
    },
    apps: {
        item: {
            icon: <appdk.UI.ICONS.Feather.Package size={48}/>,
            text: 'settings.menu.apps'
        },
        menu: (appdk, trans)=>{
            return (
                <SettingsMenuApps appdk={appdk} trans={trans}/>
            );
        }
    },
    wallpaper: {
        item: {
            icon: <appdk.UI.ICONS.Feather.Image size={48}/>,
            text: 'settings.menu.wallpaper'
        },
        menu: (appdk, trans)=>{return (
            <SettingsMenuWallpapers appdk={appdk} trans={trans}/>
        );}
    },
    device: {
        item: {
            icon: <appdk.UI.ICONS.Feather.Airplay size={48}/>,
            text: 'settings.menu.device'
        },
        menu: (appdk, trans) => {
            return (
                <SettingsMenuDevice appdk={appdk} trans={trans}/>
            );
        }
    },
    about: {
        item: {
            icon: <appdk.UI.ICONS.Feather.Info size={48}/>,
            text: 'settings.menu.about',
        },
        menu: (appdk, trans) => {
            return <SettingsMenuAbout appdk={appdk} trans={trans}/>;
        }
    }};
};

function App({intent, props, appdk}) {
    var mrtranslate = new appdk.Translator(TRANSLATIONS);
    const [target, setTarget] = React.useState('');
    let renderedlist=[];
    let renderedmenus=[];
    for (let [key, value] of Object.entries(settingsmenuitems(appdk))) {

        renderedmenus.push(<SettingsMenu appdk={appdk} key={value.item.text+'menu'} trans={mrtranslate} menuid={key} title={mrtranslate.translate(value.item.text)} istargeted={target===key} child={value.menu(appdk, mrtranslate)} goback={()=>{setTarget('');}}/>);

        renderedlist.push(<SettingsMenuItem key={value.item.text+'item'} trans={mrtranslate} color={SettingsMenuItemColor[key]} icon={value.item.icon} text={mrtranslate.translate(value.item.text)} onClick={()=>{setTarget(key);}}/>);
    }
    return (
        <div style={{
            background:'#fff',
            width:'100%',
            height:'100%',
            position:'relative'
        }}>
          {/*Main List*/}
          <div style={{
              position:'absolute',
              top:0,
              left:0,
              height:'100%',
              width:'100%'
          }}>
            <appdk.UI.Scrollbars>
              {renderedlist}
            </appdk.UI.Scrollbars>
          </div>
          {/*Menu List to slide in*/}
          <div style={{
              position:'absolute',
              top:0,
              left:'100%',
              height:'100%',
              width:'100%'
          }}>
            {renderedmenus}
          </div>
        </div>
    );
}

const LAYER = new AppLayer('Manager','icu.ccw.turtleos.settingsmanager',{
    render: ({props, intent, appdk, bridge={}}) => {
        return <App props={props} intent={intent} appdk={appdk} bridge={bridge}/>;
    },
    icon: icon,
    requests: {
       // [PERMISSION.ROOT]:true,
        [PERMISSION.MODIFY_SYSTEM_SETTINGS]:true,
        [PERMISSION.ACCESS_STORAGE]:true,
        [PERMISSION.READ_STORAGE]:true,
        [PERMISSION.WRITE_STORAGE]:true,
        [PERMISSION.RESET]:true
    },
    forceGrant: {
        // [PERMISSION.ROOT]:true,
        [PERMISSION.MODIFY_SYSTEM_SETTINGS]:true,
        [PERMISSION.ACCESS_STORAGE]:true,
        [PERMISSION.READ_STORAGE]:true,
        [PERMISSION.WRITE_STORAGE]:true,
        [PERMISSION.RESET]:true
    },
});
export {
    LAYER
}
