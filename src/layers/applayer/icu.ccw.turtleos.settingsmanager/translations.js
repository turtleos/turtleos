const TRANSLATIONS = {
    settings: {
        menu: {
            common: {
                de: 'Allgemein',
                en: 'General'
            },
            wallpaper: {
                de: 'Hintergründe',
                en: 'Wallpapers'
            },
            connectivity: {
                en: 'Connectivity',
                de: 'Verbindung'
            },
            apps: {
                en:'Apps',
                de:'Anwendungen'
            },
            device: {
                en: 'Device',
                de: 'Geräteinformation'
            },
            about: {
                en: 'About',
                de: 'Über'
            }
        },
        submenu: {
            common: {
                brightness: {
                    de:'Helligkeit',
                    en:'Brightness'
                },
                language: {
                    de:'Sprache',
                    en:'Language'
                }
            },
            connectivity: {
                network: {
                    de:'Netzwerkverbindung',
                    en:'Networkconnection'
                },
                autonetwork: {
                    de: 'Autom. Netzwerk An',
                    en: 'Network ON autom.'
                }
            },
            device: {
                uptime: {
                    de: 'Aktiv seit',
                    en: 'Uptime'
                },
                totalRuntime: {
                    de: 'Gesamte Laufzeit',
                    en: 'total device usage'
                },
                bootcount: {
                    de: 'Bootzahl',
                    en: 'Bootcount'
                },
                reset: {
                    de:'Auf Werkeinstellungen zurücksetzten',
                    en: 'Factoryreset'
                }
            },
            about: {
                credits: {
                    de:'Credits',
                    en: 'Credits'
                },
                authored: {
                    de:'von',
                    en:'by'
                },
                license: {
                    de:'Lizenzen',
                    en: 'License'
                }
            }
        },
        time: {
            hour: {
                de:'Stunden',
                en:'hours'
            }
        }
    }
};

export {
    TRANSLATIONS
}
