import {LAYER as Lattus} from './icu.ccw.turtleos.lattusbrowser/app';
import {LAYER as Manager} from './icu.ccw.turtleos.settingsmanager/app';
//import {LAYER as TurtleStore} from './icu.ccw.turtleos.turtlestore/app';
import {LAYER as TurtleShell} from './icu.ccw.turtleos.turtleshell/app';


const APPLAYERS = [
    Lattus,
    Manager,
    TurtleShell
];
export {
    APPLAYERS
}
