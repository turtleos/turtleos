import { AppLayer } from '../../turtleos-am/am';
import icon from './icon.png';
import {TRANSLATIONS} from './translations.js';
import {PERMISSION} from '../../../lib/turtleos-permissions/permissions';
import * as TPM from '../../../lib/turtleos-package-manager/packagemanager';

import React from 'react';

/*
Single App View
*/
function AppScreenshot({view, key}) {
    return (
        <div key={key} style={{
            margin:'0.4rem',
            height:'16rem',
            display:'flex',
            alignItems:'center',
            justifyContent:'center'
        }}>
          {view}
        </div>
    );
}
function AppScreenshotList({screenshots, icon, appdk}) {
    const minshots = 3;
    let screenshotList = screenshots.map((shot,index)=><AppScreenshot key={`screenshot-list-${index}`} view={<img alt={'Screenshot'} src={shot} style={{
        maxHeight:'15rem',
        maxWidth:'15rem',
        borderRadius:'15px'
    }}/>}/>);
    //generate some screenshots
    for(let i = screenshotList.length; i<minshots;i++) {
        screenshotList.push(
            <AppScreenshot key={`screenshot-list-${i}`} view={
                <div style={{
                    width:(i%2===0)?'10rem':'15rem',
                    height:(i%2===0)?'15rem':'10rem',
                    position:'relative',
                    display:'flex',
                    alignItems:'center',
                    justifyContent:'center',
                    backgroundImage:'linear-gradient(0deg, #ececec, #fdfdfd)',
                    borderRadius:'15px',
                    marginRight:'0.8rem',
                    marginLeft:'0.2rem'
                }}>
                  <img alt="Screenshot" src={icon} style={{
                      width:'5rem'
                  }}/>
                </div>
            }/>
        );
    }
    return (
        <div style={{
            position:'relative',
            width:'100%',
            height:'20rem'
        }}>
          <appdk.UI.Scrollbars>
            <div style={{
              display:'flex',
                flexDirection:'row',
                minWidth:'100%',
          }}>
              {screenshotList}
        </div>
          </appdk.UI.Scrollbars>
        </div>
    );
}

function AppInfo({installed, data, appdk, trans, onAttemptInstall}) {
    return (
        <div style={{
            width:'100%',
            display:'flex',
            flexDirection:'row',
            flexWrap:'wrap',
            alignItems:'center',
            justifyContent:'center',
        }}>
          <div style={{
              
          }}>
            <img alt="Icon" src={data.icon} style={{
                maxWidth:'5rem',
                margin:'1.2rem'
            }}/>
          </div>
          <div style={{
              fontSize:'1rem',
              display:'flex',
              flexDirection:'column',
              minWidth:'10rem',
              alignItems:'center',
              justifyContent:'center'
          }}>
            <div style={{
                fontSize:'2rem',
                fontFamily:'monospace'
            }}>
              {data.name}
            </div>
            <appdk.UI.GruvboxButton onClick={onAttemptInstall} color='blue' text={trans.translate(`app.${(installed)?'open':'install'}`).toUpperCase()}/>
          </div>
        </div>
    );
}

function AppDescription({data, trans, appdk}) {
    return (
        <div style={{
            opacity:0.7,
            margin:'0.4rem'
        }}>
          {(typeof data.description==="string")?data.description:(new appdk.Translator({
              desc:{main:data.description}
          }).translate('desc.main'))}
        </div>
    );
}

function AppOverview({data, appdk, close, trans, src}) {
    const [isInstalled, setIsInstalled] = React.useState(false);
    return (
        <div style={{
            height:'100%',
            width:'100%'
        }}>
          <div style={{
              color:'#212121',
              position:'absolute',
              margin:'0.4rem',
              top:0,
              left:0
          }} onClick={()=>close()}>
            {<appdk.UI.ICONS.Feather.ChevronLeft size={32}/>}
          </div>
          <appdk.UI.Scrollbars>
            <AppInfo data={data} appdk={appdk} trans={trans} onAttemptInstall={()=>{
                // try installing app
                TPM.installApp(src)
                    .then(d=>console.log(d))
                    .catch(e=>console.warn(e));
            }} installed={isInstalled}/>
            <AppDescription trans={trans} appdk={appdk} data={data}/>
            <AppScreenshotList appdk={appdk} screenshots={data.screenshots} icon={data.icon}/>
          </appdk.UI.Scrollbars>
        </div>
    );
}


/*
Repo Overview
 */

function RepoInfo({appdk, repoinfo}) {
    return (
        <div style={{
            width:'100%',
            background:'#ececec',
            display:'flex',
            flexDirection:'row',
            alignItems:'center',
            justifyContent:'center',
            flexWrap:'wrap',
            fontFamilly:'monospace',
            boxShadow:'0px 0px 15px -5px rgba(0, 0, 0, 0.2)'
        }}>
          <div style={{
              height:'100%',
              display:'flex',
              alignItems:'center',
              justifyContent:'center'
          }}>
            <img alt="Repo Icon" src={repoinfo.icon} style={{
                width:'5rem',
                padding:'0.8rem',
                margin:'0.8rem',
                borderRadius:'25%',
                backgroundImage: 'linear-gradient(#dcdcdc, #cccccc)'
            }}/>
          </div>
          <div style={{
              fontSize:'1.3rem',
              fontWeight:500,
              display:'flex',
              flexDirection:'column',
              padding:'0.8rem'
          }}>
            {repoinfo.name}
            <div style={{
                fontSize:'1rem',
                fontWeight:300
            }}>
              {repoinfo.description[appdk.System.getLanguage()]}
            </div>
          </div>
        </div>
    );
}

function CategoryTabBarItem({text, onClick, selected}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div onClick={onClick} style={{
            background:(hovered)?'#f2f2f2':(!selected)?'#dcdcdc':'#fcfcfc',
            borderRadius:'5px',
            transition:'background .2s',
            height:'calc(100% - 0.4rem)',
            //width:'calc(100% - 0.4rem)',
            padding:'0 0.4rem',
            margin:'0.2rem'
        }} onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)}>
          {text}
        </div>
    );
}

function CategoryTabBar({items, selected, trans}) {
    return (
        <div style={{
            width: '100%',
            background:'#dcdcdc',
            display:'flex',
            flexDirection:'row',
            flexWrap:'wrap',
            alignItems:'center',
            justifyContent:'space-evenly'
//            gridTemplateColumns:'repeat(auto-fit, minmax(5rem, auto))'
        }}>
          {Object.entries(items).map(([id, {click, text}])=>{
              return <CategoryTabBarItem selected={(id===selected)} key={id} text={trans.translate('categories.'+text).toUpperCase()} onClick={click}/>;
          })}
        </div>
    );
}

function AppListItem({data, open, close, appdk, trans, src}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div key={data.id} style={{
            display:'flex',
            flexDirection:'row',
            width:'100%',
            background:(hovered)?'#ebdbb2':'#fbf1c7',
            alignItems:'center',
            transition:'background .2s'
        }} onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)} onClick={()=>open(<AppOverview trans={trans} appdk={appdk} close={close} src={src} data={data}/>)}>
          <img alt="Icon" src={data.icon} style={{
              maxHeight:'4rem',
              margin:'0px 0.4rem'
          }}/>
          <div>
            {data.name}
          </div>
        </div>
    );
}

function RepoAppList({appdk, open, repos, repo, close, trans}) {
    const [selected, setSelected] = React.useState('all');
    // create category list
    let categories = {};
    let cats = [];
    cats.unshift('all');
    // create app list
    let items = {};
    Object.entries(repos[repo].packages).forEach(([id, data]) => {
        try {
            let manifest = JSON.parse(data.manifest);
            if(!TPM.checkAppManifest(manifest)) throw new Error("Checks failed");
            manifest.categories.forEach(cat=>{
                if(!cats.includes(cat)) {
                    cats.push(cat);
                }
                    });
            if(manifest.categories.includes(selected) || selected==='all') {
                // add app to list
                items[id]={
                    data:manifest,
                    src:data
                };
            }
        } catch(e) {
            // error occured do nothing
            console.log(e);
        }
    });
    cats.forEach(cat=>{
        categories[cat]={
            text: cat,
            click: ()=>setSelected(cat)
        };
    });
    return (
        <div style={{}}>
          <CategoryTabBar items={categories} trans={trans} selected={selected}/>
        <appdk.UI.Scrollbars style={{height:'100%',position:'absolute'}}>
            <div style={{
                display:'flex',
                flexDirection:'column',
                alignItems:'center',
                height:'100%',
                width:'100%',
                position:'relative'
            }}>
            {Object.entries(items).map(([id, data], index, arr)=>{
                let render = [];
                if(index>0 && index<arr.length-1) {
                    // render seperator
                    render.push(
                        <div style={{
                            height:'2px',
                            background:'rgba(0,0,0,0.4)',
                            width:'90%'
                        }} key={`seperator-${id}`}></div>
                    );
                }
                // render app item
                render.push(<AppListItem appdk={appdk} trans={trans} close={close} open={open} data={data.data} src={data.src}/>);
                return render;
            })}
        </div>
          </appdk.UI.Scrollbars>
        </div>
    );
}

function BackButton({close, appdk}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div onClick={()=>close()} style={{
            position:'absolute',
            top:'0.4rem',
            left:'0.4rem',
            padding:'0.4rem',
            transition:'color .2s',
            margin:'0.4rem',
            color:(!hovered)?'grey':'white'
        }} onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)}>
          <appdk.UI.ICONS.Feather.ChevronLeft size={32}/>
        </div>
    );
}

function RepoOverview({appdk, open, close, repos, repo, trans}) {
    return (
        <div style={{
            background:'#fbf1c7',
            height:'100%',
            width:'100%',
            position:'absolute'
        }}>
          <BackButton appdk={appdk} close={close}/>
          <RepoInfo trans={trans} appdk={appdk} repoinfo={repos[repo].info.repo}/>
          <RepoAppList trans={trans} open={open} close={close} appdk={appdk} repo={repo} repos={repos}/>
        </div>
    );
}

/*
Repo List
 */
function RepoListItem({text, icon, id, onClick, style={}}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div key={id} style={{
            width:'15rem',
            height:'15rem',
            background:(hovered)?'#d5c4a1':'#ebdbb2',
            boxShadow:'0px 0px 15px -5px rgba(0,0,0,0.2)',
            overflow:'hidden',
            borderRadius:'15px',
            display:'flex',
            flexDirection:'column',
            alignItems:'center',
            justifyContent:'space-evenly',
            transition:'background .2s',
            ...style
        }}
             onMouseEnter={()=>setHovered(true)}
             onMouseLeave={()=>setHovered(false)}
             onClick={()=>onClick()}>
          <img alt={id} src={icon} style={{
              width:'40%'
          }}/>
          <div>{text}</div>
        </div>
    );
}
function RepoList({appdk, repos, open, close, trans}) {
    return (
        <appdk.UI.Scrollbars>
          <div style={{
              height:'100%',
              width:'100%',
              background:'#fbf1c7',
              display:'grid',
              alignItems:'center',
              justifyContent:'space-evenly',
              gridGap:'0.8rem'
          }}>
            {Object.entries(repos).map(([key, value]) => {
                return (
                    <RepoListItem text={value.info.repo.name} icon={value.info.repo.icon} id={key} onClick={()=>open(<RepoOverview trans={trans} appdk={appdk} open={open} close={close} repos={repos} repo={key}/>)}/>
                );
            })}
            <div style={{
                width:'90%',
                height:'2px',
                background:'black',
                borderRadius:'15px',
                opacity:0.2,
                marginLeft:'5%'
            }}></div>
            <RepoListCustomGroup group={{
                add:{
                    click:()=>{},
                    text:trans.translate('menu.add'),
                    view: <appdk.UI.ICONS.Feather.Plus size={32}/>
                },
                search: {
                    click:()=>{},
                    text: trans.translate('menu.search'),
                    view: <appdk.UI.ICONS.Feather.Search size={32}/>
                },
                tide: {
                    click:()=>{},
                    text: trans.translate('menu.tide'),
                    view: <appdk.UI.ICONS.Feather.Code size={32}/>
                },
                options: {
                    click:()=>{},
                    text: trans.translate('menu.options'),
                    view: <appdk.UI.ICONS.Feather.Edit size={32}/>
                }
            }}/>
          </div>
        </appdk.UI.Scrollbars>
    );
}
function RepoListCustomGroup({group}) {
    return (
        <div style={{
            display:'grid',
            gridTemplateRows:'1fr 1fr',
            gridTemplateColumns:'1fr 1fr',
            width:'15rem',
            height:'15rem'
        }}>
          {Object.entries(group).map(([id, item])=>{
              return <RepoListCustomItem id={id} onClick={item.click} view={item.view} text={item.text} style={{
                  width:'7rem',
                  height:'7rem'
              }}/>;
          })}
        </div>
    );
}
function RepoListCustomItem({text,view, onClick, id, style={}}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div key={id} style={{
            width:'15rem',
            height:'15rem',
            background:(hovered)?'#d5c4a1':'#ebdbb2',
            boxShadow:'0px 0px 15px -7px rgba(0,0,0,0.2)',
            overflow:'hidden',
            borderRadius:'15px',
            display:'flex',
            flexDirection:'column',
            alignItems:'center',
            justifyContent:'space-evenly',
            transition:'background .2s',
            ...style
        }}
             onMouseEnter={()=>setHovered(true)}
             onMouseLeave={()=>setHovered(false)}
             onClick={()=>onClick()}>
          {view}
          <div>{text}</div>
        </div>
    );
}
function StoreError({text}) {
    return (
        <div>
          
        </div>
    );
}

function PanelView({view}) {
    const [offset, setOffset] = React.useState([0,'100%']);
    React.useEffect(()=>{
        setTimeout(()=>setOffset([0,0]),100);
    },[]);
    return (
        <div style={{
            position:'absolute',
            top:offset[0],
            left:offset[1],
            width:'100%',
            height:'100%',
            transition:'left .2s',
            background:'#fbf1c7'
        }}>
          {view}
        </div>
    );
}

function App({appdk, intent, bridge}) {
    var mrtranslate = new appdk.Translator(TRANSLATIONS);
    const [repos, setRepos] = React.useState({});
    const [views, setViews] = React.useState([]);

    
    function addView(view) {
        let v = views.slice();
        v.push(<PanelView view={view}/>);
        setViews(v);
    }
    function removeView() {
        let v = views.slice();
        v.pop();
        setViews(v);
    }

    React.useEffect(()=>{
        TPM.updateAllRepos()
            .then(rep=>{
                setRepos(rep);
            })
            .catch(e=>console.log(e));
    },[]);
    // Resolve intents
    React.useEffect(()=>{
        return appdk.AM.IntentResolver({
            intent: intent,
            bridge: bridge
        },(intentd)=>{
            //intent found
        });
    },[]);
    return (
        <div style={{
            position:'absolute',
            top:0,
            left:0,
            width:'100%',
            height:'100%',
            display:'flex',
            flexDirection:'column',
            justifyContent:'center',
            alignItems:'center',
            background:'#fbf1c7',
        }}>
          <RepoList trans={mrtranslate} appdk={appdk} close={removeView} repos={repos} open={addView}/>
          {views}
        </div>
    );
}


const LAYER = new AppLayer('TurtleStore','icu.ccw.turtleos.turtlestore',{
    render: ({props, intent, appdk, bridge={}}) => {
        return <App props={props} intent={intent} appdk={appdk} bridge={bridge}/>;
    },
    icon: icon,
    requests: {
        // [PERMISSION.ROOT]:true,
        [PERMISSION.MODIFY_SYSTEM_SETTINGS]:true,
        [PERMISSION.ACCESS_STORAGE]:true,
        [PERMISSION.READ_STORAGE]:true,
        [PERMISSION.WRITE_STORAGE]:true,
        [PERMISSION.RESET]:true
    },
    forceGrant: {
        // [PERMISSION.ROOT]:true,
        [PERMISSION.MODIFY_SYSTEM_SETTINGS]:true,
        [PERMISSION.ACCESS_STORAGE]:true,
        [PERMISSION.READ_STORAGE]:true,
        [PERMISSION.WRITE_STORAGE]:true,
        [PERMISSION.RESET]:true
    },
});
export {
    LAYER
}
