import { AppLayer } from '../../turtleos-am/am';
import icon from './icon.png';
import {TRANSLATIONS} from './translations.js';
import {PERMISSION} from '../../../lib/turtleos-permissions/permissions';
import {TYPES as TSH_TYPES} from '../../tsh/tsh.js';
import { AppdkUI } from '../../../lib/turtleos-appdk/appdk-ui';

import React from 'react';

const std = {
    [TSH_TYPES.MESSAGE]: ({cont="", conf={
        style:{},
        child:{style:{}}
    }})=>{
        if(Array.isArray(cont) && cont.length===1) {
        cont=cont[0];
        }
        if(!Array.isArray(cont)) cont=cont.toString();
        if(cont.indexOf("col")===0 && Array.isArray(cont)) {
            // color mode
            let parts = cont.slice(1);
            parts = parts.map(([text, color])=>{
                let cl = "";
                if(color.indexOf("#")===0) {
                    //hex
                    cl=color;
                } else {
                    // select from palette
                    cl = AppdkUI.COLORS.GRUVBOX[color];
                    if(!cl) cl=AppdkUI.COLORS.GRUVBOX['white_dark'];
                    cl=cl[1];
                }
                return (
                    <span style={{color: cl,...conf.child.style}}>{text}</span>
                );
            });
            return (
                <div style={conf.parent.style}>
                <div
                  style={conf.style}
                >
                  {parts}
                </div>
                </div>
            );
        } else {
        cont=cont.toString();
        cont+="\n";
        cont = cont.replaceAll("\n","\u00A0");
        return (
            <div>
                  {cont}
            </div>
        );
        }
    },
    [TSH_TYPES.ERROR]: ({cont=""})=>{
        if(cont==="") {
            cont="Command not found";
        }
        cont=`\u00A0${cont}\u00A0`;
        return (
            <div>
              <span style={{
                  color: AppdkUI.COLORS.GRUVBOX.red[2]
              }}>{cont}</span>
            </div>
        );
    },
    [TSH_TYPES.CMD]: ({cmd=""})=>{
        return (
            <div style={{
                opacity:0.9
            }}>
              <br/>
              <span style={{
                  color:AppdkUI.COLORS.GRUVBOX.aqua[1]
              }}>+++  </span><span style={{
                  color: AppdkUI.COLORS.GRUVBOX.black_dark[4]
              }}>{cmd}</span>
              <br/>
            </div>
        );
    }
};

function App({intent, props, appdk}) {
    //var mrtranslate = new appdk.Translator(TRANSLATIONS);
    const tsh = appdk.System.getState(state=>state.tsh);
    const gruv = appdk.UI.COLORS.GRUVBOX;
    const scrollbar = React.useRef();

    const [history, setHistory] = React.useState({hist:[]});
    const [stdout, setOut] = React.useState({std:[]});
    const [cmd, setCMD] = React.useState('');
    let old = "";
    let index = -1;
    const [enabled, setEnabled] = React.useState(false);
    const [config, setConfig] = React.useState({
        "print_cmd": true
    });

    const stream = {
        push: (data) => {
            if(scrollbar.current) scrollbar.current.scrollToBottom();
            if(data.type===TSH_TYPES.BEGIN) {
                setEnabled(false);
            } else if(data.type===TSH_TYPES.END) {
                setEnabled(true);
            } else if(data.type === TSH_TYPES.EVENT) {
                switch(data.event) {
                case "clear":
                    setOut({std:[]});
                    break;
                case "config-read":
                    return config[data.data.key];
                case "config-write":
                    let c = {
                        ...config,
                        [data.data.key]:data.data.value
                    };
                    setConfig(c);
                    break;
                default:
                }
                    return {};
            } else {
            let o = stdout.std;
            o.push(data);
            setOut({
                std:o});
            }
            return {};
        }
    };

    React.useEffect(()=>{
        tsh.execute('("neofetch")', stream);
    },[]);

    return (
        <div style={{
            background:gruv.black_dark[0],
            width:'calc(100% - 0.4rem)',
            height:'calc(100% - 0.4rem)',
            padding:'0.2rem',
            position:'relative'
        }}>
          <appdk.UI.Scrollbars
            dref={scrollbar}
          >
            <div className="stdout" style={{
                width:'calc(100% - 2rem)',
                whiteSpace:'pre',
                padding:'1rem',
                fontFamily:'monospace',
                textAlign:'left',
                color: gruv.white_dark[3],
            }}>
        {stdout.std.map(item=>{
                  let render = std[item.type];
                  if(!render) return (<div></div>);
            return render(item);
                  })}
            </div>
            <div className="stdin" style={{
                width:'calc(100% - 1rem)',
                display:'flex',
                flexDirection:'row',
                color: (enabled)?gruv.orange[1]:gruv.aqua[2],
                padding:'0.2rem 0',
                transition: 'color .1s'
            }}>
              <span>+++ </span><input
                                 name="command"
                                 onChange={(e)=>setCMD(e.target.value)}
                                 type="text"
                                 value={cmd}
                                 style={{
                                     background:'transparent',
                                     color: (enabled)?gruv.white_dark[3]:gruv.black_dark[5],
                                     border:'none',
                                     flex:1,
                                     marginLeft:'0.4rem',
                                     transition: 'color .1s',
                                     outline:'none'
                                 }}
                                 placeholder='('
                                 onKeyDown={(e)=>{
                                     if(enabled) {
                                     if (e.key === 'Enter') {
                                         //Enterkey pressed
                                         if(config['print_cmd']) {
                                         //add command to out
                                         let o = stdout.std;
                                         o.push({
                                             type:TSH_TYPES.CMD,
                                             cmd: cmd
                                         });
                                         setOut({
                                             std:o});
                                         }
                                         // save in history
                                         let hist = history.hist;
                                         hist.push(cmd);
                                         setHistory({hist:hist});
                                         //execute
                                         tsh.execute(cmd, stream);
                                         setCMD("");
                                     } else if (e.key === 'ArrowUp') {
                                         //history up
                                         if(index<0) {
                                             index=-1;
                                             old=cmd;
                                         }
                                         index+=1;
                                         let c = "";
                                         if(index<0) {
                                             c=old;
                                         } else {
                                             c=history.hist[index];
                                         }
                                         setCMD(c);
                                     } else if (e.key === 'ArrowDown') {
                                         //history down
                                         if(index===-1) {
                                             old=cmd;
                                         }
                                         index-=1;
                                         if(index<0) index=-1;
                                         let c = "";
                                         if(index<0) {
                                             c=old;
                                         } else {
                                             c=history.hist[index];
                                         }
                                         setCMD(c);
                                     }}
                                 }}
                               />
            </div>
        </appdk.UI.Scrollbars>
        </div>
    );
}

const LAYER = new AppLayer('Shell','icu.ccw.turtleos.turtleshell',{
    render: ({props, intent, appdk, bridge={}}) => {
        return <App props={props} intent={intent} appdk={appdk} bridge={bridge}/>;
    },
    icon: icon,
    requests: {
       // [PERMISSION.ROOT]:true,
        [PERMISSION.MODIFY_SYSTEM_SETTINGS]:true,
        [PERMISSION.ACCESS_STORAGE]:true,
        [PERMISSION.READ_STORAGE]:true,
        [PERMISSION.WRITE_STORAGE]:true,
        [PERMISSION.RESET]:true
    },
    forceGrant: {
        // [PERMISSION.ROOT]:true,
        [PERMISSION.MODIFY_SYSTEM_SETTINGS]:true,
        [PERMISSION.ACCESS_STORAGE]:true,
        [PERMISSION.READ_STORAGE]:true,
        [PERMISSION.WRITE_STORAGE]:true,
        [PERMISSION.RESET]:true
    },
});
export {
    LAYER
}
