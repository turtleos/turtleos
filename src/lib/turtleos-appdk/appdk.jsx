
//import { useDispatch, useSelector } from 'react-redux';

import { pm } from '../../layers/turtleos-pm/pm';
import { IO } from '../turtleos-io/io';
import { isWallpaper, WALLPAPERS } from '../../assets/turtle-wallpaper-collection/wallpapers';
import { translateMe } from '../turtleos-translations/translations';
import { PERMISSION } from '../turtleos-permissions/permissions';
import { defaultBootConfig } from '../turtleos-rootfs/rootfs';
import { NetworkManager } from '../../layers/turtleos-nm/networkmanager';
import { AppdkUI } from './appdk-ui';

import { useSelector } from 'react-redux';

function APPDK({ /*String*/ appid, /*PermissionManager*/ permmgr }) {
    /*
      FS Helper functions
    */
    function isPathRoot( path ) {
        if ( path.length < 2 || path.match(new RegExp("/","g")) < 2) return true;
        return false;
    };
    function getAppRoot() {
        return `/usr/apps/${appid.split(".").join('/')}`;
    }
    function hasAccess( path, act ) {
        if(isPathRoot(path) && !permmgr.hasPermission(PERMISSION.ROOT).permission) return false;
        if(['w','m','d'].indexOf(act)>-1 && !permmgr.hasPermission(PERMISSION.WRITE_STORAGE).permission) return false;
        if(act==='r'&&!permmgr.hasPermission(PERMISSION.READ_STORAGE).permission) return false;
        let ar = getAppRoot();
        if (path.indexOf(ar)===0) return true;
        return false;
    }

    return {
        AM: {
            startApp: (id, intent={}) => {
                let am = pm.getState().am;
                if(Object.keys(am.windows).indexOf(id)!==-1) {
                    am.am.getAppById(id).dispatch(new Event('focus'));
                    am.am.getAppById(id).dispatch(new CustomEvent('intent',{
                        detail: intent
                    }));
                    if(am.hidden[appid]) am.am.toggleHideApp(id);
                } else {
                    am.am.startApp(pm.getState().platform,id, intent);
                }

            },
            IntentResolver: ({bridge,intent},callback) => {
                if(intent!=={}) {
                    // app started with intent
                    callback(intent);
                }
                let listen = bridge.addEventListener('intent',(d)=>{
                    // called with intent
                    if(d.detail!=={}) callback(d.detail);
                });
                return ()=>{
                    bridge.removeEventListener('intent', listen);
                };
            }
        },
        /*
          FileSystem
          permission_min: ACCESS_STORAGE
        */
        FS : Object.assign({},(permmgr.hasPermission(PERMISSION.ACCESS_STORAGE).permission||permmgr.hasPermission(PERMISSION.ROOT).permission)?{

            ...Object.assign({},(permmgr.hasPermission(PERMISSION.RESET).permission)?{
                //may reset
                reset: ()=>{
                    return pm.dispatch({
                        type:'install:reset',
                        payload:{}
                    });
                }
            }:{}),
            // Used to write content to a file
            writeFile: ( path, content ) => {
                if (hasAccess(path,'w')) return { error: true, message: 'No Permission', code: 0 };
                return pm.getState().vfs.writeFileSync(path, content);
            },
            // used to clear a folder by passing content={}
            writeFolder: ( path, content ) => {
                if (hasAccess(path, 'w')) return { error: true, message: 'No Permission', code: 0 };
                return pm.getState().vfs.writeFolderSync(path, content);
            },
            // used to delete a file from the fs
            deleteFile: ( path ) => {
                if (hasAccess(path, 'd')) return { error: true, message: 'No Permission', code: 0 };
                return pm.getState().vfs.deleteFileSync(path);
            },
            // used to delete a folder from the fs
            deleteFolder: ( path ) => {
                if (hasAccess(path, 'd')) return { error: true, message: 'No Permission', code: 0 };
                return pm.getState().vfs.deleteFolderSync(path);
            },
            // used to create a new file
            makeFile: ( path, name, {content, permission, type} ) => {
                if ( hasAccess(path, 'm')) return { error: true, message: 'No Permission', code: 0 };
                return pm.getState().vfs.makeFileSync(path, name, {type, content, permission});
            },
            // user to create a new folder
            makeFolder: ( path, name, {content, permission, type} ) => {
                if (hasAccess(path, 'm')) return { error: true, message: 'No Permission', code: 0 };
                return pm.getState().vfs.makeFolderSync(path, name, {content, permission});
            },
            // used to read the contents of a file
            readFile: ( path ) => {
                if (hasAccess(path, 'r')) return { error: true, message: 'No Permission', code: 0 };
                return pm.getState().vfs.readFileSync(path);
            },
            // used to list contents of directory
            readFolder: ( path ) => {
               if (hasAccess(path, 'r')) return { error: true, message: 'No Permission', code: 0 };
               return pm.getState().vfs.readFolderSync(path);
            },
            // used to get root path ( has to be appended in front of every path)
            getAppRoot: () => {
                return getAppRoot();
            }
        }:{}),
        /*
          Translations
        */
        Translator: function(db) {
            this.translate = (path) => {
                return translateMe(db, path);
            };
        },
        System: {
            Settings: Object.assign({},(permmgr.hasPermission(PERMISSION.MODIFY_SYSTEM_SETTINGS).permission || permmgr.hasPermission(PERMISSION.ROOT).permission)?{
                // add settings modification
                setBrightness: (d) => {
                    let res =  IO('brightness').setBrightness(d);
                    if (res) {
                        //save to disk
                        let cont = {};
                        try {
                            let file = pm.getState().vfs.readFileSync('/buildconf');
                            cont=JSON.parse(file.data.content);
                        } catch(e) {
                            cont=defaultBootConfig;
                        }
                        pm.getState().vfs.writeFileSync('/buildconf',JSON.stringify({
                            ...cont,
                            brightness: pm.getState().io.display.brightness
                        }));
                    }
                    return res;
                },
                setLanguage: (lng) => {
                    let res = pm.getState().lang.setLanguage(lng);
                    if(res) {
                        let cont = {};
                        try {
                            let file = pm.getState().vfs.readFileSync('/buildconf');
                            cont=JSON.parse(file.data.content);
                        } catch(e) {
                            cont=defaultBootConfig;
                        }
                        pm.getState().vfs.writeFileSync('/buildconf',JSON.stringify({
                            ...cont,
                            lang: lng
                        }));
                    }
                    return res;
                },
                setWallpaper: (id) => {
                    if (isWallpaper(id)) {
                        pm.dispatch({
                            type:'wallpaper:change',
                            payload:{
                                wallpaper:id
                            }
                        });
                        let cont = {};
                        try {
                            let file = pm.getState().vfs.readFileSync('/buildconf');
                            cont=JSON.parse(file.data.content);
                        } catch(e) {
                            cont=defaultBootConfig;
                        }
                        pm.getState().vfs.writeFileSync('/buildconf',JSON.stringify({
                            ...cont,
                            wallpaper: id
                        }));
                        return true;
                    } else {
                        return false;
                    }
                },
                setNetwork: (state) => {
                    let res = IO('network')[(state)?'on':'off']();
                    if(res) {
                        //save new state
                        let cont = {};
                        try {
                            let file = pm.getState().vfs.readFileSync('/buildconf');
                            cont=JSON.parse(file.data.content);
                        } catch(e) {
                            cont=defaultBootConfig;
                        }
                        pm.getState().vfs.writeFileSync('/buildconf',JSON.stringify({
                            ...cont,
                            network: state
                        }));
                    }
                    return res;
                },
                setAutoNetwork: (state) => {
                    let res = IO('autonetwork')[(state)?'on':'off']();
                    if(res) {
                        //save new state
                        let cont = {};
                        try {
                            let file = pm.getState().vfs.readFileSync('/buildconf');
                            cont=JSON.parse(file.data.content);
                        } catch(e) {
                            cont=defaultBootConfig;
                        }
                        pm.getState().vfs.writeFileSync('/buildconf',JSON.stringify({
                            ...cont,
                            autonetwork: state
                        }));
                    }
                    return res;
                },
                getVFS:()=>{
                    return pm.getState().vfs;
                }
            }:{}),
            //only add getters
            //WALLPAPERS
            getCurrentWallpaper:()=>{
                return pm.getState().wallpaper;
            },
            getWallpaper:(id) => {
                if(isWallpaper(id)) return WALLPAPERS[id];
                return {};
            },
            isWallpaper:(id)=>{
                return isWallpaper(id);
            },
            getWallpapers:()=>{
                return Object.keys(WALLPAPERS);
            },
            getAllWallpapers:()=>{
                return WALLPAPERS;
            },
            //NETWORKING
            isOnline:()=>{
                return pm.getState().io.network;
            },
            //BRIGHTNESS
            getBrightness:()=>{
                return pm.getState().io.display.brightness;
            },
            //LANG
            getLanguage:()=>{
                return pm.getState().lang.lang;
            },
            //PLATFORM
            getPlatform:()=>{
                return pm.getState().platform;
            },
            getCurrentTime:()=>{
                return pm.getState().internal.clock;
            },
            getState:(permmgr.hasPermission(PERMISSION.MODIFY_SYSTEM_SETTINGS).permission)?useSelector:{},
        },
        UI: AppdkUI,
        Network: Object.assign({},(permmgr.hasPermission(PERMISSION.NETWORK).permission)?NetworkManager():{})
    };
}

export {
    APPDK
}
