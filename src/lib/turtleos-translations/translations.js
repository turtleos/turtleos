import { pm } from '../../layers/turtleos-pm/pm';

const translations = {
    common: {
        installharddisk: {
            de: 'Auf Festplatte installieren',
            en: 'Install to Harddisk'
        }
    },
    dialog: {
        shutdown: {
            attemptshutdown: {
                de: 'Möchtest du TurtleOS wirklich herunterfahren?',
                en: 'Do you really want to shutdown TurtleOS'
            },
            doshutdown: {
                de: 'Speichern & Herunterfahren',
                en: 'Saving Data...\n Shutting down'
            }
        },
        construction: {
            de:'Diese Seite befindet sich momentan in bearbeitung',
            en: 'We are currently working on this page '
        },
        sleeping: {
            de:'Hier passiert nichts...',
            en:'Nothing is happening here'
        },
        noconnection: {
            de:'Keine Netzwerkverbindung',
            en:'No network connection'
        }
    },
    console: {
        subtitle: {
            de: 'Nichts außer das Zirpen der Grillen',
            en: 'Nothing but the chirping of crickets'
        },
        somethingisoff: {
            de: 'Mhh! Es scheint als seie irgendwas schiefgegangen... \n Ich tu einfach so als sei nichts passiert',
            en: 'Something seems off! I\'ll just pretend nothing happened at continue as normal'
        }
    },
    error: {
        notranslation: {
            de: 'Hier sollte Text in deiner Sprache stehen - er ist allerdings noch nicht übersetzt',
            en: 'There actually should be text in your language here - but it seems like it hasn\'t been translated yet'
        },
        othertabopened: {
            de: 'Du hast TurtleOS bereits in einem anderen Tab geöffnet. \n Entweder du wechselst zu ihm oder schließt ihn \n - mir egal, aber vorher passiert hier nichts',
            en: 'TurtleOS is already booted in a diffrent tab. \n Either close that tab or switch back \n - I don\'t care, but before you\'re not getting into action I won\'t either'
        }
    },
    datetime: {
        days: {
            1: {
                de:'Montag',
                en:'Monday'
            },
            2: {
                de:'Dienstag',
                en:'Tuesday'
            },
            3: {
                de:'Mittwoch',
                en:'Wednesday'
            },
            4: {
                de:'Donnerstag',
                en:'Thursday'
            },

            5: {
                de:'Freitag',
                en:'Friday'
            },

            6: {
                de:'Samstag',
                en:'Saturday'
            },

            0: {
                de:'Sonntag',
                en:'Sunday'
            },


        }
    },
    network: {
        noconnection: {
            de: 'Keine Netzwerkverbindung',
            en: 'No Networkconnection'
        }
    },
    languages: {
        de: {
            de:'Deutsch',
            en:'German'
        },
        en: {
            de:'Englisch',
            en:'English'
        }
    },
    system: {
        license: {
            en: 'TurtleOS is build with webtechnologies, and therefore contains npm modules which might be licensed under diffrent licenses. Please check the package.json for more details',
            de:'TurtleOS wird von Web Technoligien betrieben und enthält mehrere NPM Module die  unterschiedliche Lizenzen benötigen. Sieh dir die package.json Datei an für mehr Information'
        }
    }
};

const defaultLanguage = 'en';
const languages = [ 'de', 'en'];


const LANG = {
    lang: defaultLanguage,
    default: defaultLanguage,
    languages: languages,
    isLanguage: ( lang ) => {
        if(languages.indexOf(lang) > -1) return true;
        return false;
    },
    setLanguage: ( lang ) => {
        if (!LANG.isLanguage(lang)) return false;
        pm.dispatch({
            type:'lang:change',
            payload: {
                lang: lang
            }
        });
        return true;
    },
    translate: ( path ) => {
        let tmp = {...translations};
        for(let i of path.split('.')) {
            tmp = tmp[i];
            if(tmp===undefined) tmp={};
        }
        tmp=tmp[pm.getState().lang.lang];
        if(tmp==={}||tmp===undefined) tmp=LANG.translate('error.notranslation');
        return tmp;
    }
};

function translateMe(transl, path) {
    let lang = pm.getState().lang.lang;
    let tmp = {...transl};
    for(let i of path.split('.')) {
        tmp = tmp[i];
        if(tmp===undefined) tmp={};
    }
    tmp=tmp[lang];
    if(tmp==={}||tmp===undefined) tmp=LANG.translate('error.notranslation');
    return tmp;

}

export {
    LANG,
    translateMe
}
