class PermissionManager {
    constructor({requested, granted}) {
        this.requested=requested;
        this.granted=granted;
    }
    exists(/*PERMISSIONS*/ permid) {
        if(Object.values(PERMISSION).includes(permid) && Object.values(PERMISSION).includes(permid)) return true;
        return false;
    }
    hasPermission(/*PERMISSIONS*/ permid ) {
        let error = 3;
        let knownpermission = false;
        if (!this.exists(permid)) {
            error=2;
            knownpermission=false;
        } else if(this.requested[permid]===undefined || this.granted[permid] === undefined) {
            error=0;
            knownpermission=false;
        } else if(this.requested[permid]!==this.granted[permid]) {
            error=1;
            knownpermission=true;
        } else if (this.requested[permid] && this.granted[permid]) {
            error = -1;
            knownpermission = true;
        }
        return {
            permission: (error===-1)?true:false,
            error: error,
            data: {
                permid: permid,
                knownpermission: knownpermission,
                requested: this.requested[permid],
                granted: this.granted[permid],
            }
        };
        }
}
const base = 'turtleos.permission';
const PERMISSION = {
    ROOT: `${base}.ROOT`,
    RESET: `${base}.FACTORY_RESET`,
    NETWORK: `${base}.ACCESS_NETWORK`,
    ACCESS_STORAGE: `${base}.ACCESS_STORAGE`,
    READ_STORAGE: `${base}.READ_STORAGE`,
    WRITE_STORAGE: `${base}.WRITE_STORAGE`,
    MODIFY_SYSTEM_SETTINGS: `${base}.MODIFY_SYSTEM_SETTINGS`,
    MAY_REQUEST_REBOOT: `${base}.MAY_REQUEST_REBOOT`
};

export {
    PERMISSION,
    PermissionManager
}
