import {
    Permission,
    VirtFS,
    Folder,
    File
} from './virtfs.js';

//DEMO
//initialise filesystem
let xyz = VirtFS({
    isroot: false,
    filesystem: {
        etc: new Folder({content:{},permission: new Permission({root:{},user:{canMake:true, canRead: true,}})})
    }
});

//Create new Files
console.log(xyz.makeFolderSync('/etc','etc',{
    content: {},
    permission: new Permission({root:{},user:{canRead: true}})
}));
console.log(xyz.makeFileSync('/etc','hostname',{
    content: 'My Awesome PC',
    type: 'txt',
    permission: new Permission({root:{},user:{canRead: true}})
}));
console.log(xyz.makeFolderSync('/etc','system',{
    content: {},
    permission: new Permission({root:{},user:{canRead: true}})
}));


//Read Files / Folders
console.log(xyz.readFileSync('/etc/hostname'));
console.log(xyz.readFolderSync('/etc/system'));

//start Subroot
let mp = xyz.clone('/etc');
console.log(mp.readFolderSync('/'));
console.log(xyz.readFolderSync('/etc'));

