/*
  VirtFS Basic
   - a basic Virtual Filesystem
*/
class VirtFSBasic {
    constructor() {
        //Setup filesystem
        this._vfs={};
    }
    merge(virtfs) {
        //merge current filesystem with new one
        this._vfs = Object.assign(this._vfs, virtfs);
    }
    get getAll() {
        return this._vfs;
    }
    getSync(path) {
        let p = path;
        if(p[0]==="/") p=p.substring(1);
        if(p[p.length-1]==="/") p=p.substring(0,p.length-1);
        let parts = p.split("/");
        let tmp = Object.assign({},this._vfs);
        let c = 0;
        for(let part of parts) {
            c++;
            tmp=tmp[part];
            if(c<=parts.length-1) {
                tmp=tmp.content;
            }
        }
        if(tmp===undefined) {
            return {
                found: false
            };
        } else {
            return {
                found: true,
                data: tmp
            };
        }
    }
    writeSync(path, content) {
        let p = path;
        if(p[0]==="/") p=p.substring(1);
        if(p[p.length-1]==="/") p=p.substring(0,p.length-1);
        let parts = p.split("/");

        let data = this.getSync(path);
        if(!data.found) {
            return {
                found: false,
                error: true
            };
        }
        if(data.type==="folder") {
            return {
                found: true,
                error: true,
                type: 'file',
            };
        }
        let newData = Object.assign({},data).data;
        newData.content = content;

        let c = 0;
        let tmp = Object.assign({},this._vfs);
        for(let part of parts) {
            c++;
            tmp=tmp[part];
            if(c<=parts.length-1) {
                tmp=tmp.content;
            }
            if(c===parts.length) {
                //Found file
                tmp=newData;
            }
        }
        return {
            found: true,
            error: false
        };
    }
    newSync(path, name, filefolder) {
        let p = path;
        if(p[0]==="/") p=p.substring(1);
        if(p[p.length-1]==="/") p=p.substring(0,p.length-1);
        let parts = p.split("/");

        let data = this.getSync(path);
        if(!data.found) {
            return {
                found: false,
                error: true
            };
        }
        if(data.type==="folder") {
            return {
                found: true,
                error: true,
                type: 'file',
            };
        }

        let c = 0;
        let tmp = Object.assign({},this._vfs);
        for(let part of parts) {
            c++;
            tmp=tmp[part];
            if(c<=parts.length-1) {
                tmp=tmp.content;
            }
            if(c===parts.length) {
                tmp.content[name]=filefolder;
            }
        }
        return {
            created: true,
            error: false
        };
    }
    deleteSync(path) {
        let p = path;
        if(p[0]==="/") p=p.substring(1);
        if(p[p.length-1]==="/") p=p.substring(0,p.length-1);
        let parts = p.split("/");

        let data = this.getSync(path);
        if(!data.found) {
            return {
                found: false,
                error: true
            };
        }
        if(data.type==="folder") {
            return {
                found: true,
                error: true,
                type: 'file',
            };
        }

        let c = 0;
        let tmp = Object.assign({},this._vfs);
        for(let part of parts) {
            c++;
            tmp=tmp[part];
            if(c<parts.length-1) {
                tmp=tmp.content;
            }
            if(c===parts.length-1) {
                delete tmp.content[parts[c]];
            }
        }
        return {
            found: true,
            error: false
        };
    }
    export() {
        return this._vfs;
    }
}

//SOME BASIC DATA TYPES
function Permission({
    root,
    user
}) {
    let permRoot = [];
    let permUser = [];

    if(root.canRead) permRoot.push("r");
    if(root.canWrite) permRoot.push("w");
    if(root.canDelete) permRoot.push("d");
    if(root.canMake) permRoot.push("m");
    if(user.canRead) permUser.push("r");
    if(user.canWrite) permUser.push("w");
    if(user.canDelete) permUser.push("d");
    if(user.canMake) permUser.push('m');

    return {
        root: permRoot,
        user: permUser
    };
}

function File({
    bType,
    content,
    permission
}) {
    let d = new Date();
    return {
        type: 'file',
        props: {
            dates: {
                created: d.getTime(),
                modified: d.getTime()
            },
            bType: (!bType)?'data':bType,
            permission:(!permission)?{root:[],user:[]}:permission
        },
        content: (!content)?'':content
    };
}
function Folder({
    content,
    permission
}) {
    let d = new Date();
    return {
        type: 'folder',
        props: {
            dates: {
                created: d.getTime(),
                modified: d.getTime()
            },
            permission:(!permission)?{root:[],user:[]}:permission
        },
        content: (!content)?{}:content
    };
}
/*
  export  {
        File,
        Folder,
        Permission,
        VirtFSBasic
    };
*/

/*
VirtFS
 - advanced Virtual Filesystem
 */

function VirtFS({
    isroot,
    approot,
    filesystem
}) {
    let vfs = new VirtFSBasic();
    vfs.merge(filesystem);
    let user = (isroot)?'root':'user';

    //Helper Functions
    const pathToArray = (path) => {
        let p = path;
        if(p[0]==="/") p=p.substring(1);
        if(p[p.length-1]==="/") p=p.substring(0,p.length-1);
        let parts = p.split("/");
        return parts;
    };
    const hasPermission = (item, perm) => {
        return (item.props.permission[user].indexOf(perm)>=0);
    };
    const readSync = (path) => {
        let data = vfs.getSync(path);
        if(data.found&&!data.error) {
            // No error occured
            if(hasPermission(data.data, "r")) {
                //User may access
                return {
                    found: true,
                    error: false,
                    permission: true,
                    data: data.data
                };
            } else {
                return {
                    found: true,
                    error: true,
                    permission: false
                };
            }
        } else {
            return {
               ...data,
            };
        }
    };
    const makeSync = (path, name, type, config) => {
        if(path==="/") {
            return {
                error: true,
                permission: false
            };
        }
        let d = vfs.getSync(path);
        if(!d.found || d.error) {
            return {
                found: false,
                error: true
            };
        }
        if(!hasPermission(d.data, "m")) {
            return {
                error: true,
                permission: false,
                found: true
            };
        }
        let data = undefined;
        if(type==="folder") {
            data = new Folder(config);
        } else if(type==='file') {
            data = new File(config);
        }
        return vfs.newSync(path, name, data);
    };
    const writeSync = (path, type, content) => {
        if(path==="/") {
            return {
                error: true,
                permission: false
            };
        }
        let d = vfs.getSync(path);
        if(!hasPermission(d.data, "w")) {
            return {
                error: true,
                permission: false
            };
        }
        if(d.data.type!==type) {
            return {
                error: true,
                type: false
            };
        }
        return vfs.writeSync(path, content);
    };
    const deleteSync = (path) => {
        if(path==="/") {
            return {
                error: true,
                permission: false
            };
        }
        let d = vfs.getSync(path);
        if(!d.found||d.error) {
            return d;
        }
        if(hasPermission(d.data, 'd')) {
            return vfs.deleteSync(path);
        } else {
            return {
                error: true,
                permission: false
            };
        }
    };

    //return data
    let funcs = {
        //FILE and FOLDER reading
        readFileSync: (path) => {
            let d = readSync(path);
            if(d.error||!d.found) {
                return d;
            }
            if(d.data.type==='file') {
                return d;
            } else {
                return {
                    error: true,
                    file: false
                };
            }
        },
        readFolderSync: (path) => {
            let d = readSync(path);
            if(d.error||!d.found) {
                return d;
            }
            if(d.data.type==='folder') {
                return d;
            } else {
                return {
                    error: true,
                    folder: false
                };
            }
        },

        //FILE and FOLDER creation
        makeFileSync: (path, name, {type, content, permission}) => {
            let d = makeSync(path, name, 'file', {
                content: content,
                bType: type,
                permission: permission});
            if(d.error) {
                return d;
            }
            return {
                error: false,
                created: true,
            };
        },
        makeFolderSync: (path, name, {content, permission}) => {
            let d = makeSync(path, name, 'folder', {
                content: content,
                permission: permission});
            if(d.error) {
                return d;
            }
            return {
                error: false,
                created: true,
            };
        },

        //FILE and FOLDER writing
        writeFileSync: (path, content) => {
            return writeSync(path, 'file', content);
        },
        writeFolderSync: (path, content) => {
            return writeSync(path, 'folder', content);
        },

        //FILE and FOLDER deletion
        deleteFileSync: (path) => {
            return deleteSync(path);
        },
        deleteFolderSync: (path) => {
            return deleteSync(path);
        },

        export: () => {
            return vfs.export();
        },

        //clone to other apps
        clone: (chroot) => {
            return {
                readFileSync: (path) => {
                    return funcs.readFileSync(chroot+path);
                },
                readFolderSync: (path) => {
                    return funcs.readFolderSync(chroot+path);
                },

                makeFileSync: (path, name, {type, content, permission}) => {
                    return funcs.makeFileSync(chroot + path, name, {type, content, permission});
                },
                makeFolderSync: (path, name, {content, permission}) => {
                    return funcs.makeFolderSync(chroot + path, name, {content, permission});
                },

                writeFileSync: (path, content) => {
                    return funcs.writeFileSync(chroot + path, content);
                },
                writeFolderSync: (path, content) => {
                    return funcs.writeFolderSync(chroot + path, content);
                },

                deleteFileSync: (path) => {
                    return funcs.deleteFileSync(chroot + path);
                },
                deleteFolderSync: (path) => {
                    return funcs.deleteFolderSync(chroot + path);
                },

                clone: (proot) => {
                    return funcs.clone(chroot + proot);
                },
                export: () => {
                    return funcs.export();
                }
            };
        }
    };
    return {
        readFileSync: funcs.readFileSync,
        readFolderSync: funcs.readFolderSync,

        makeFileSync: funcs.makeFileSync,
        makeFolderSync: funcs.makeFolderSync,

        writeFileSync: funcs.writeFileSync,
        writeFolderSync: funcs.writeFolderSync,

        deleteFileSync: funcs.deleteFileSync,
        deleteFolderSync: funcs.deleteFolderSync,

        clone: funcs.clone,

        pta: pathToArray,

        export: funcs.export
    };
}

export {
    VirtFS,
    Permission,
    Folder,
    File
}

