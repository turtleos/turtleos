import { pm } from '../../layers/turtleos-pm/pm';

function IO(type) {
    const dispatch = pm.dispatch;

    if (type === "brightness") {
        return {
            setBrightness: ({ perc=50, inc=5, dec=5, type="perc"}) => {
                let bright = pm.getState().io.display.brightness;
                if (type==="inc") bright+=inc;
                if (type==="dec") bright-=dec;
                if (type==="perc") bright=perc;

                if (bright => 10 && bright <= 100) {
                    dispatch({
                        type: 'io:update-depth',
                        payload: {
                            interface: 'display',
                            key: 'brightness',
                            value: bright
                        }
                    });
                    return true;
                }
                return false;
            },
            getBrightness: ()=>{
                return pm.getState().io.display.brightness;
            }
        };
    }

    return {
        toggle: () => {
            dispatch({
                type: 'io:update',
                payload: {
                    interface: type,
                    state: !pm.getState().io[type]
                }
            });
            return  !pm.getState().io[type];
        },
        on: () => {
            dispatch({
                type: 'io:update',
                payload: {
                    interface: type,
                    state: true
                }
            });
            return true;
        },
            off: () => {
                dispatch({
                    type: 'io:update',
                    payload: {
                        interface: type,
                        state: false
                    }
                });
                return true;
            },
    };
}

export {
    IO
}
