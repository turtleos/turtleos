import React from 'react';

const json2react = (json, prefix="") => {

    // check valid element
    if(!json.tag) throw new Error('No Tag found');
    if (!json.children) json.children=[];
    if (!Array.isArray(json.children)) throw new Error('Elements children must be contained in array');

    // convert children
    let children = json.children.map(widget=>{
        if (typeof widget === "string") {
            // plain text given as child -> no convertion needed
            return widget;
        }
        if (typeof widget !== "object") {
            throw new Error('child has to be string or object');
        }
        // convert children
        return json2react(widget, prefix);
    });

    // create config from data
    let config = Object.assign({}, json);
    delete config.tag;
    delete config.children;
    // bridge childs to children
    if (!!config.childs) config.children=config.childs;

    // prefix sensitive data
    const sensitive = ["id", "class", "name"];
    sensitive.forEach(prop => {
        if(!!config[prop]) config[prop]=prefix+config[prop];
    });

    // return element
    return React.createElement(
        json.tag,
        config,
        ...children
    );
};

export {
    json2react
}
